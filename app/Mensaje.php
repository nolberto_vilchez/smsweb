<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mensajes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['campana_id', 'numero_celular', 'mensaje', 'estado_mensaje', 'estado_fecha', 'estado','user_id_create'];

    
}
