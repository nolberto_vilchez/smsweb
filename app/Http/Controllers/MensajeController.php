<?php

namespace App\Http\Controllers;

use App\Campana;
use App\CampanaResultado;
use App\Empresa;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

class MensajeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function cargarmensajes($empresa_id,$campana_id){

        return view('mensaje.cargarmensaje', compact('campana_id','empresa_id'));
    }

    public function listamensajes($empresa_id,$campana_id){
        return view('mensaje.cargarmensaje', compact('campana_id','empresa_id'));
    }

    public function reporte($campana_id,$token,$empresa_id){
        $mensajes = Mensaje::where('campana_id',$campana_id)->where('token',$token)->get();
        $campana = Campana::find($campana_id)->getAttribute('mensaje_base');
        $empresa = Empresa::find($empresa_id)->getAttribute('nombre');

        //dd($campana);
        $table = "<table border='1'>";

        $table.="<tr><th border='1' bgcolor='#dceaf7'>Cliente</th><th border='1' bgcolor='#dceaf7'>Destinatario</th> <th border='1' bgcolor='#dceaf7'>Fecha envío</th><th border='1' bgcolor='#dceaf7'>Fecha recepción</th><th border='1' bgcolor='#dceaf7'>estado</th><th border='1' bgcolor='#dceaf7'>Mensaje</th><th border='1' bgcolor='#dceaf7'>token</th><th border='1' bgcolor='#dceaf7'>n sms</th></tr>";
        foreach($mensajes as $mensaje){
            $sms_array = json_decode($mensaje->mensaje);
            $sms = $campana;
            $estado_m='';

            if($mensaje->estado_mensaje==1 or $mensaje->estado_mensaje==null){
                $estado_m = 'Pendiente';
            }elseif($mensaje->estado_mensaje==2){
                $estado_m = 'Enviado';
            }elseif($mensaje->estado_mensaje==3){
                $estado_m = 'Recibido';
            }

            for($i=0; $i<count($sms_array); $i++){
                $sms = str_replace('{'.($i).'}',$sms_array[$i],$sms);
            }

            $table.="<tr><td border='1'>".$empresa."</td><td border='1'>".$mensaje->numero_celular."</td> <td border='1'>".$mensaje->estado_fecha."</td><td border='1'>".$mensaje->fecha_recepcion."</td><td border='1'>".$estado_m."</td><td border='1'>".$sms."</td><td border='1'>".$mensaje->token."</td><td border='1'>".$mensaje->n_sms."</td></tr>";

        }
        $table.="</table>";

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.date('Y-m-d_his').'_reporte_mensajes.xls"');
        header('Cache-Control: max-age=0');

        echo utf8_decode($table);

    }

    public function reporte_campana($campana_id,$empresa_id){
        $mensajes = Mensaje::where('campana_id',$campana_id)->get();
        $campana = Campana::find($campana_id)->getAttribute('mensaje_base');
        $empresa = Empresa::find($empresa_id)->getAttribute('nombre');

        //dd($campana);
        $table = "<table border='1'>";

        $table.="<tr><th border='1' bgcolor='#dceaf7'>Cliente</th><th border='1' bgcolor='#dceaf7'>Destinatario</th> <th border='1' bgcolor='#dceaf7'>Fecha envío</th><th border='1' bgcolor='#dceaf7'>Fecha recepción</th><th border='1' bgcolor='#dceaf7'>estado</th><th border='1' bgcolor='#dceaf7'>Mensaje</th><th border='1' bgcolor='#dceaf7'>token</th><th border='1' bgcolor='#dceaf7'>n sms</th></tr>";
        foreach($mensajes as $mensaje){
            $sms_array = json_decode($mensaje->mensaje);
            $sms = $campana;
            $estado_m='';

            if($mensaje->estado_mensaje==1 or $mensaje->estado_mensaje==null){
                $estado_m = 'Pendiente';
            }elseif($mensaje->estado_mensaje==2){
                $estado_m = 'Enviado';
            }elseif($mensaje->estado_mensaje==3){
                $estado_m = 'Recibido';
            }

            for($i=0; $i<count($sms_array); $i++){
                $sms = str_replace('{'.($i).'}',$sms_array[$i],$sms);
            }

            $table.="<tr><td border='1'>".$empresa."</td><td border='1'>".$mensaje->numero_celular."</td> <td border='1'>".$mensaje->estado_fecha."</td><td border='1'>".$mensaje->fecha_recepcion."</td><td border='1'>".$estado_m."</td><td border='1'>".$sms."</td><td border='1'>".$mensaje->token."</td><td border='1'>".$mensaje->n_sms."</td></tr>";

        }
        $table.="</table>";

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.date('Y-m-d_his').'_reporte_mensajes.xls"');
        header('Cache-Control: max-age=0');

        echo utf8_decode($table);

    }

    public function listarmensajes(Request $request, $empresa_id, $campana_id){

        $model_campana = Campana::find($campana_id);
        $mensaje_base = $model_campana->mensaje_base;

        $keyword = $request->get('search');
        $perPage = 100;

        if (!empty($keyword)) {

            $mensaje = Mensaje::where('campana_id', '=', $campana_id)
                ->where(function($query) use ($keyword){
                    $query->where('numero_celular', 'LIKE', "%$keyword%")
                        ->orWhere('mensaje', 'LIKE', "%$keyword%")
                        ->orWhere('estado_mensaje', 'LIKE', "%$keyword%")
                        ->orWhere('estado_fecha', 'LIKE', "%$keyword%")
                        ->orWhere('estado', 'LIKE', "%$keyword%");
                })
                ->paginate($perPage);
        } else {
            $mensaje = Mensaje::where('campana_id', '=', $campana_id)->paginate($perPage);
        }

        return view('mensaje.listar', compact('mensaje', 'campana_id','empresa_id','mensaje_base'));
    }

    public function storemensajes(Request $request,$empresa_id,$campana_id){


        $all_row = $request['all_row'];
        $all_row = json_decode($all_row);
        $mensaje = $request['mensaje'];

        $cant_columnas = count($all_row[0]);

        foreach( $all_row as $clave=>$valor){

            $sms = $mensaje;
            for($i=1; $i<$cant_columnas; $i++){
                $sms = str_replace('{'.($i-1).'}',$valor[$i],$sms);
            }
                $modelo = new Mensaje();
                $modelo->mensaje = $sms;
                $modelo->numero_celular = $valor[0];
                $modelo->campana_id = $campana_id;
                $modelo->user_id_create = Auth::user()->id;
                $modelo->estado = 1;
                $modelo->save();
        }


        return view('mensaje.enviadomensaje',compact('empresa_id','campana_id'));


    }

    public function enviarmensajes(Request $request,$empresa_id,$campana_id)
    {
        $requestData = $request->all();

        $mensaje = $requestData['mensaje'];

        $path = Input::file('archivo')->getRealPath();

        $datos = Excel::selectSheetsByIndex(0)->load($path,function($reader){

                }, 'UTF-8')->noHeading()->get();

        $one_row = $datos->first()->toArray();
        $all_row = $datos->toArray();

        //dd($all_row);

        $all_row = json_encode($all_row);

        /*mostrando la previa de un mensajer*/
        /*una sola fila*/

        $sms1 = $mensaje;
        for($i=1; $i<count($one_row); $i++){
            $sms1 = str_replace('{'.($i-1).'}',$one_row[$i],$sms1);
        }
        echo $sms1."<br>";



return view('mensaje.previewmensaje',compact('sms1','mensaje','all_row','empresa_id','campana_id'));





    }



    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $mensaje = Mensaje::where('campana_id', 'LIKE', "%$keyword%")
                ->orWhere('numero_celular', 'LIKE', "%$keyword%")
                ->orWhere('mensaje', 'LIKE', "%$keyword%")
                ->orWhere('estado_mensaje', 'LIKE', "%$keyword%")
                ->orWhere('estado_fecha', 'LIKE', "%$keyword%")
                ->orWhere('estado', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $mensaje = Mensaje::paginate($perPage);
        }

        return view('mensaje.index', compact('mensaje'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('mensaje.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Mensaje::create($requestData);

        return redirect('mensaje')->with('flash_message', 'El mensaje fue agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mensaje = Mensaje::findOrFail($id);

        return view('mensaje.show', compact('mensaje'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mensaje = Mensaje::findOrFail($id);

        return view('mensaje.edit', compact('mensaje'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $mensaje = Mensaje::findOrFail($id);
        $mensaje->update($requestData);

        return redirect('mensaje')->with('flash_message', 'El mensaje fue actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Mensaje::destroy($id);

        return redirect('mensaje')->with('flash_message', 'El mensaje fue eliminado');
    }




}
