<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Contacto;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contacto = Contacto::where('empresa_id', 'LIKE', "%$keyword%")
                ->orWhere('nombre', 'LIKE', "%$keyword%")
                ->orWhere('apellido', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('cargo', 'LIKE', "%$keyword%")
                ->orWhere('estado', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $contacto = Contacto::paginate($perPage);
        }

        return view('contacto.index', compact('contacto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('contacto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['user_id_create'] = Auth::user()->id;

        Contacto::create($requestData);

        return redirect('contacto/empresa/'.$requestData['empresa_id'].'/lista')->with('flash_message', 'El contacto fue agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contacto = Contacto::findOrFail($id);

        return view('contacto.show', compact('contacto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contacto = Contacto::findOrFail($id);

        return view('contacto.edit', compact('contacto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        $contacto = Contacto::findOrFail($id);
        $contacto->update($requestData);

        return redirect('contacto/empresa/'.$request->empresa_id.'/lista')->with('flash_message','El contacto fue actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $contacto = Contacto::findOrFail($id);
        Contacto::destroy($id);

        return redirect('contacto/empresa/'.$contacto->empresa_id.'/lista')->with('flash_message', 'El cliente fue eliminado');
    }

    public function crearcontacto($empresa_id)
    {

        return view('contacto.create',['empresa_id'=>$empresa_id]);
    }

    public function editarcontacto($contacto_id,$empresa_id)
    {
        $contacto = Contacto::findOrFail($contacto_id);
        return view('contacto.edit', compact('contacto','empresa_id','contacto_id'));
    }

    public function listacontacto(Request $request,$empresa_id)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {


            $contacto = DB::table('contactos')
                ->where('empresa_id','=',$empresa_id)
                ->where(function($query) use ($keyword){
                    $query->where('nombre','like',"%$keyword%")
                        ->orWhere('apellido','like',"%$keyword%")
                        ->orWhere('telefono', 'LIKE', "%$keyword%")
                        ->orWhere('email', 'LIKE', "%$keyword%")
                        ->orWhere('cargo', 'LIKE', "%$keyword%")
                        ->orWhere('estado', 'LIKE', "%$keyword%");
                })
                ->paginate($perPage);


        } else {
            $contacto = Contacto::where('empresa_id','=',$empresa_id)->paginate($perPage);
        }

        //dd($contacto);

        return view('contacto.index', compact('contacto','empresa_id'));

      //  return view('contacto.index',['empresa_id'=>$empresa_id]);
    }

}
