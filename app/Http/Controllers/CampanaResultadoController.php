<?php

namespace App\Http\Controllers;

use App\CampanaResultado;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CampanaResultadoController extends Controller
{
    public function listar($campana_id,$empresa_id)
    {
        $resultados =  CampanaResultado::select('token',DB::raw('SUM(n_sms) as por_enviar'))->where('id_campana',$campana_id)->groupBy('token')->get();

        return view('campanaresultado.index',compact('campana_id','empresa_id','resultados'));
    }
}
