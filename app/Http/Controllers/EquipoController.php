<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Equipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EquipoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $equipo = Equipo::where('marca', 'LIKE', "%$keyword%")
                ->orWhere('modelo', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->orWhere('proveedor', 'LIKE', "%$keyword%")
                ->orWhere('androidversion', 'LIKE', "%$keyword%")
                ->orWhere('activo', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $equipo = Equipo::paginate($perPage);
        }

        return view('equipo.index', compact('equipo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('equipo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $token = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

        //$resultado =  $this->verificar_token($token);

        //dd($resultado);
        //TODO falta el verificador del TOKEN

        $requestData = $request->all();
        $requestData['token'] = $token;
        $requestData['client_id'] = Auth::user()->client_id;
        $requestData['user_id_create'] = Auth::user()->id;


        Equipo::create($requestData);


        /* -- envio api -- */

        //TODO para terminar esta parte hay que agregarle el client_id


            $ch = curl_init( 'http://www.insite.pe/sms/api/request/putToken' );
            # Setup request to send json via POST.
            $payload = json_encode( array( "token"=> $token,"id"=>Auth::user()->client_id ) );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            # Return response instead of printing.
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            # Send request.
            $result = curl_exec($ch);
            curl_close($ch);
            # Print response.
            //echo "<pre>$result</pre>";
        //die;


        /*
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.insite.pe/sms/api/request/putToken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"token\"\r\n\r\n".$token."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_id\"\r\n\r\n".$cliente_id."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: b0067489-c181-86a5-b0c5-fc978e08c440"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

       */


        return redirect('equipo')->with('flash_message', 'El equipo se agregó correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $equipo = Equipo::findOrFail($id);

        return view('equipo.show', compact('equipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $equipo = Equipo::findOrFail($id);

        return view('equipo.edit', compact('equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $equipo = Equipo::findOrFail($id);
        $equipo->update($requestData);

        return redirect('equipo')->with('flash_message', 'Equipo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Equipo::destroy($id);

        return redirect('equipo')->with('flash_message', 'Equipo deleted!');
    }

    /**
     * @param $token
     * @return mixed
     */

    //TODO verificador de token
    public function verificar_token($token)
    {
        $resp = Equipo::Where('token', '=', $token)->get();
        $aa = $resp->toArray();
        return $aa[0]['id'];

    }
}
