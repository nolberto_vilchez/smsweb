<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Equipo;
use App\Empresa;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['equipos']  = Equipo::where('client_id', Auth::user()->client_id)->where('activo', "1")->count();
        $data['empresas'] = Empresa::where('client_id', Auth::user()->client_id)->where('activo', "1")->count();

        return view('home', compact('data'));
    }

}
