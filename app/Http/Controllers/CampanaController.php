<?php

namespace App\Http\Controllers;

use App\CampanaResultado;
use App\Equipo;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Campana;
use App\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;


class CampanaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cancelar($campana_id)
    {
        $mensajes = Mensaje::where('estado_fb',0)->where('campana_id',$campana_id)->get();

       // dd($mensajes);

        foreach($mensajes as $mensaje){
            $model = Mensaje::find($mensaje->id);
            $model->estado_fb=3;
            $model->estado_mensaje = 4;
            $model->save();
        }

        $campana = Campana::find($campana_id);
        $campana->estado = 5;
        $campana->save();

        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $campana = Campana::where('empresa_id', 'LIKE', "%$keyword%")
                ->orWhere('nombre', 'LIKE', "%$keyword%")
                ->orWhere('fecha_envio', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $campana = Campana::orderBy('fecha_envio','asc')->paginate($perPage);
        }

        return view('campana.index', compact('campana'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('campana.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function previocampana(Request $request){

        $requestData = $request->all();

        $mensaje_base = $requestData['mensaje_base'];
        $empresa_id = $requestData['empresa_id'];
        $requestData['user_id_create'] = Auth::user()->id;
        $tipo_envio = $requestData['tipo_envio'];

        $path = Input::file('archivo')->getRealPath();

        $datos = Excel::selectSheetsByIndex(0)->load($path,function($reader){
        }, 'UTF-8')->noHeading()->get();

        $one_row = $datos->first()->toArray();
        $all_row = $datos->toArray();
        $count_mensajes = count($all_row);
        $all_row = json_encode($all_row);

        /*guardo la campana
        $model_campana = new Campana();
        $model_campana->mensaje_base = $mensaje;
        $model_campana->nombre = $requestData['nombre'];
        $model_campana->fecha_envio = $requestData['fecha_envio'];
        $model_campana->user_id_create = Auth::user()->id;
        $model_campana->estado = 1;
        $model_campana->empresa_id = $empresa_id;
        $model_campana->save();
        */

        /*armando el mensaje de prueba*/

        $sms1 = $mensaje_base;
        for($i=1; $i<count($one_row); $i++){
            $sms1 = str_replace('{'.($i-1).'}',$one_row[$i],$sms1);
        }


        $campana_array = [];
        $campana_array['mensaje_base'] =$mensaje_base;
        $campana_array['nombre'] = $requestData['nombre'];
        $campana_array['fecha_envio'] = $requestData['fecha_envio'];
        $campana_array['user_id_create'] = Auth::user()->id;
        $campana_array['empresa_id'] = $empresa_id;

        /*equipos*/

        $equipos = Equipo::where('activo',1)->get(); // TODO no se ha considerado el clien_id , hay     ue agregarle cuandi el client id este mejor estructurado

        return view('campana.previo', compact('sms1','all_row','mensaje_base','empresa_id','campana_array','count_mensajes','tipo_envio','equipos'));


    }

    public function envioinmediato(Request $request){

        $requestData = $request->all();

        $mensaje_base = $requestData['mensaje_base'];
        $empresa_id = $requestData['empresa_id'];
        $requestData['user_id_create'] = Auth::user()->id;

        $all_row = json_decode($requestData['all_row']);

        /* ======================= guardo la campana ======================== */
        $model_campana = new Campana();
        $model_campana->mensaje_base = $mensaje_base;
        $model_campana->nombre = $requestData['nombre'];
        $model_campana->fecha_envio = date('Y-m-d H:i:s');
        $model_campana->user_id_create = Auth::user()->id;
        $model_campana->estado = $requestData['estado'];
        $model_campana->empresa_id = $requestData['empresa_id'];
        $model_campana->save();

        /* ================== guardando mensajes ====================== */

        $num_mensajes = 0;

        foreach($all_row as $key=>$value){

            $columnas = [];


            for($i=1; $i<count($all_row[0]); $i++){
                array_push($columnas,$value[$i]);
            }

            //dd($all_row[0]);
            if(count($all_row[0])==1 or $all_row[0][1]==null){
                $columnas2 = '';
            }else{
                $columnas2 = json_encode($columnas);
            }

            $model_mensaje = new Mensaje();
            $model_mensaje->mensaje = $columnas2;
            $model_mensaje->numero_celular = $value[0];
            $model_mensaje->user_id_create = Auth::user()->id;
            $model_mensaje->estado = 1;
            $model_mensaje->campana_id = $model_campana->id;
            $model_mensaje->save();

            $num_mensajes++;

        }

        /* ================ actualizo en campanas el numero de menmsajes============== */

        $model_campana_update = Campana::find($model_campana->id);
        $model_campana_update->mensajes_total= $num_mensajes;
        $model_campana_update->estado = 3;
        $model_campana_update->save();


        /*  =========================  proceso de envío ======================= */

        $cant_equipos = count($requestData['equipo']);
        $equipos = $requestData['equipo'];

        $reparto = $this->reparto($num_mensajes,$cant_equipos);

        $array_fb = [
            "nombre" => $model_campana_update->nombre,
            "id"=> $model_campana_update->id,
            "espera"=>5,
            "estado"=>1,
        ];


        if($reparto['n_mensajes']>12){
            $reparto['n_mensajes']=12;
        }


        foreach($equipos as $k=>$equipo) {

           // if($k==0){
                $offset =  $k * $reparto['n_mensajes'];
                $limit =  $reparto['n_mensajes'];


               /* if(($k+1)==count($equipos)) {
                    $limit = $reparto['n_mensajes'];
                   // $limit = $reparto['n_mensajes'] + $reparto['residuo'];
                }*/

                $mensajes = DB::table('mensajes')->where('campana_id',$model_campana->id)->where('estado',1)
                    ->offset($offset)->limit($limit)->get();


                $array_sms= [];

                $equipo_token = Equipo::find($equipo)->getAttribute('token');

                foreach($mensajes as $mensaje){

                    $array_sms[] = [
                        "id" => $mensaje->id,
                        "mensaje" => $this->textoSMS($mensaje->mensaje, $mensaje_base),
                        "celular" => $mensaje->numero_celular
                    ];

                    $mensaje_update = Mensaje::find($mensaje->id);
                    $mensaje_update->token = $equipo_token;
                    $mensaje_update->estado_fb = 1;
                    $mensaje_update->save();

                }

                $array_fb['conteo'] = $limit;
                $array_fb['mensajes'] = $array_sms;




                $urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/".$equipo_token.".json";


                $data = json_encode($array_fb);

                $curl = curl_init($urlFirebase);

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST" );
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER,['Content-Type:application/json']);

                $retorno = curl_exec($curl);

                curl_close($curl);

                $toke_firebase = json_decode($retorno);

                $tiempo_espera = $array_fb['espera'];
                $agregado = $tiempo_espera*$limit;
                $ahora = date('Y-m-d H:i:s');


                $registro = new CampanaResultado();
                $registro->id_campana = $model_campana->id;
                $registro->token = $equipo_token;
                $registro->id_firebase = $toke_firebase->name;
                $registro->fecha_termino = date('Y-m-d H:i:s', strtotime($ahora." +".$agregado." seconds")) ;
                $registro->n_sms = $limit;
                $registro->estado_envio = 1;
                $registro->save();
           // }



        }

        return redirect('campana/empresa/'.$empresa_id.'/lista')->with('flash_message', 'La campaña fue enviada');

    }

    public function actualizarcampana(Request $request, $campana_id, $empresa_id){

        $requestData = $request->all();

        if(!isset($requestData['archivo'])){
            $campana = Campana::findOrFail($campana_id);
            $campana->update($requestData);

            return redirect('campana/empresa/'.$request->empresa_id.'/lista')->with('flash_message', 'La campaña fue actualizada');

        }else{

            /* previsualización */
            $mensaje_base = $requestData['mensaje_base'];
            $empresa_id = $requestData['empresa_id'];
            $requestData['user_id_create'] = Auth::user()->id;

            $path = Input::file('archivo')->getRealPath();

            $datos = Excel::selectSheetsByIndex(0)->load($path,function($reader){
            }, 'UTF-8')->noHeading()->get();

            $one_row = $datos->first()->toArray();
            $all_row = $datos->toArray();
            $count_mensajes = count($all_row);
            $all_row = json_encode($all_row);

            $sms1 = $mensaje_base;
            for($i=1; $i<count($one_row); $i++){
                $sms1 = str_replace('{'.($i-1).'}',$one_row[$i],$sms1);
            }



            $campana_array = [];
            $campana_array['mensaje_base'] =$mensaje_base;
            $campana_array['nombre'] = $requestData['nombre'];
            $campana_array['fecha_envio'] = $requestData['fecha_envio'];
            $campana_array['user_id_create'] = Auth::user()->id;
            $campana_array['empresa_id'] = $empresa_id;

            return view('campana.previoupdate', compact('sms1','all_row','mensaje_base','campana_id','empresa_id','campana_array','count_mensajes'));
        }

    }


    public function store(Request $request)
    {
        $requestData = $request->all();

        $mensaje = $requestData['mensaje_base'];
        $empresa_id = $requestData['empresa_id'];
        $requestData['user_id_create'] = Auth::user()->id;

        $all_row = json_decode($requestData['all_row']);

        /*guardo la campana */
        $model_campana = new Campana();
        $model_campana->mensaje_base = $mensaje;
        $model_campana->nombre = $requestData['nombre'];
        $model_campana->fecha_envio = $requestData['fecha_envio'];
        $model_campana->user_id_create = Auth::user()->id;
        $model_campana->estado = $requestData['estado'];
        $model_campana->empresa_id = $requestData['empresa_id'];
        $model_campana->save();

        //ultimo id de la camapaña guardada
        $campana_last_id = $model_campana->id;


        /* guardando mensajes */

        $num_mensajes = 0;


        foreach($all_row as $key=>$value){

            $columnas = [];


            for($i=1; $i<count($all_row[0]); $i++){
                array_push($columnas,$value[$i]);
            }

            //dd($all_row[0]);
            if(count($all_row[0])==1 or $all_row[0][1]==null){
                $columnas2 = '';
            }else{
                $columnas2 = json_encode($columnas);
            }

            $model_mensaje = new Mensaje();
            $model_mensaje->mensaje = $columnas2;
            $model_mensaje->numero_celular = $value[0];
            $model_mensaje->user_id_create = Auth::user()->id;
            $model_mensaje->estado = 1;
            $model_mensaje->campana_id = $model_campana->id;
            $model_mensaje->save();

            $num_mensajes++;

        }

        /* actualizo en campanas el numero de menmsajes */
        $model_campana_update = Campana::find($model_campana->id);
        $model_campana_update->mensajes_total= $num_mensajes;
        $model_campana_update->save();




        return redirect('campana/empresa/'.$requestData['empresa_id'].'/lista')->with('flash_message', 'La campaña fue agregada');

        //return view('mensaje.enviadomensaje',compact('empresa_id','campana_last_id'));
    }


    public function previoactualizar(Request $request){
        $requestData = $request->all();

        $mensaje = $requestData['mensaje_base'];
        $empresa_id = $requestData['empresa_id'];
        $requestData['user_id_create'] = Auth::user()->id;

        $all_row = json_decode($requestData['all_row']);


      //  dd($requestData);

        /* actualizar campana */
        $model_campana = Campana::find($requestData['campana_id']);
        $model_campana->mensaje_base = $mensaje;
        $model_campana->nombre = $requestData['nombre'];
        $model_campana->fecha_envio = $requestData['fecha_envio'];
        $model_campana->user_id_create = Auth::user()->id;
        $model_campana->estado = $requestData['estado'];
        $model_campana->empresa_id = $requestData['empresa_id'];
        $model_campana->save();

        /* borrar mensajes */

        Mensaje::where('campana_id',$requestData['campana_id'])->delete();


        /* crear mensajes */

        $num_mensajes = 0;
        foreach($all_row as $key=>$value){

            $columnas = [];
            array_push($columnas,$value[1],$value[2]);

            $columnas2 = json_encode($columnas);

            $model_mensaje = new Mensaje();
            $model_mensaje->mensaje = $columnas2;
            $model_mensaje->numero_celular = $value[0];
            $model_mensaje->user_id_create = Auth::user()->id;
            $model_mensaje->estado = 1;
            $model_mensaje->campana_id = $model_campana->id;
            $model_mensaje->save();

            $num_mensajes++;

        }

        /* actualizo en campanas el numero de menmsajes */
        $model_campana_update = Campana::find($model_campana->id);
        $model_campana_update->mensajes_total= $num_mensajes;
        $model_campana_update->save();

        return redirect('campana/empresa/'.$request->empresa_id.'/lista')->with('flash_message', 'La campaña fue actualizada');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campana = Campana::findOrFail($id);

        return view('campana.show', compact('campana'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function editarcampana($id,$empresa_id)
    {
        $campana = Campana::findOrFail($id);
        $campana_id = $id;

        return view('campana.edit', compact('campana','empresa_id','campana_id'));
    }
    public function editar2campana($id,$empresa_id)
    {
        $campana = Campana::findOrFail($id);
        $campana->estado = 1;
        $campana->save();

       $campana_id = $id;
        return view('campana.edit', compact('campana','empresa_id','campana_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();

        //dd($requestData['archivo']);

        $campana = Campana::findOrFail($id);
        $campana->update($requestData);

        return redirect('campana/empresa/'.$request->empresa_id.'/lista')->with('flash_message', 'La campaña fue actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $campana = Campana::findOrFail($id);
        Campana::destroy($id);


       // return redirect('campana')->with('flash_message', 'Campana deleted!');
        return redirect('campana/empresa/'.$campana->empresa_id.'/lista')->with('flash_message', 'La campaña fue eliminada');
    }

    public function crearcampana($empresa_id)
    {
        $crear = 1;
        return view('campana.create',compact('empresa_id','crear'));
    }

  /*  public function editarcampana($campana_id,$empresa_id)
    {
        $campana = Campana::findOrFail($campana_id);
        return view('campana.edit', compact('campana','empresa_id','campana_id'));
    }*/

    public function listacampana(Request $request,$empresa_id)
    {
        $keyword = $request->get('search');
        $perPage = 50;

        if (!empty($keyword)) {


            $campana = DB::table('campanas')
                ->where('empresa_id','=',$empresa_id)
                ->where(function($query) use ($keyword){
                    $query->where('nombre','like',"%$keyword%")->orWhere('mensaje_base','like',"%$keyword%");

                })
                ->orderBy('fecha_envio','asc')
                ->paginate($perPage);


        } else {
            $campana = Campana::where('empresa_id','=',$empresa_id)->orderBy('fecha_envio','asc')->paginate($perPage);
        }

        //dd($campana);

        return view('campana.index', compact('campana','empresa_id'));

        //  return view('contacto.index',['empresa_id'=>$empresa_id]);
    }


    public function cambiarestado($campana_id){
        $campana = Campana::find($campana_id);
        $estado = $campana->estado;

        if($estado == 1){
            $campana->estado = 2;
        }else if($estado == 2){
            $campana->estado = 1;
        }

        $campana->save();

        return back();
    }


    public function load($campana,$empresa,$estado=4,$token=0)
    {

        if($token==0){
            $resultados = CampanaResultado::where('id_campana',$campana)->get();

            foreach($resultados as $resultado){

                $urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/".$resultado->token."/".$resultado->id_firebase.".json";

                //dd($urlFirebase);

                $data = '{"estado":'.$estado.'}';

                $curl = curl_init($urlFirebase);

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH" );
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER,['Content-Type:application/json']);

                $returno = curl_exec($curl);
                curl_close($curl);

                // dd($returno);

            }

        }else{

            //$result = CampanaResultado::where('id_campana',$campana)->where('token',$token)->first();
            $resultados = CampanaResultado::where('id_campana',$campana)->where('token',$token)->get();

            foreach($resultados as $resultado) {
                //$urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/".$result->token."/".$result->id_firebase.".json";
                $urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/" . $resultado->token . "/" . $resultado->id_firebase . ".json";

                //dd($urlFirebase);

                $data = '{"estado":' . $estado . '}';

                $curl = curl_init($urlFirebase);

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);

                $returno = curl_exec($curl);
                curl_close($curl);
            }
        }

        return view('campana.load', compact('campana','empresa'));
    }




    /* ============ funciones internas ============== */
    public function textoSMS($jsonSMS, $templateSMS){


        if($jsonSMS==null or $jsonSMS=='' or $jsonSMS=='[null]'){
            $sms = $templateSMS;
        }else{
            $all_row = json_decode($jsonSMS);
            $mensaje = $templateSMS;

            $cant_columnas = count($all_row);

            $sms = $mensaje;
            for($i=0; $i<$cant_columnas; $i++){
                $sms = str_replace('{'.($i).'}',$all_row[$i],$sms);
            }
        }

        return $sms;

    }


    public function reparto($n_mensajes, $n_equipos){
        $numero = $n_mensajes/$n_equipos;
        $entera = intval($numero);
        $decimal =  $numero - $entera;
        $resto = $n_mensajes - ($entera*$n_equipos);

        return ['n_mensajes'=>$entera,'residuo'=>$resto];
    }





}
