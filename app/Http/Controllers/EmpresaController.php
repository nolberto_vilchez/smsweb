<?php

namespace App\Http\Controllers;

use App\Contacto;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 50;

        if (!empty($keyword)) {
            $empresa = Empresa::where('nombre', 'LIKE', "%$keyword%")
                ->orWhere('ruc', 'LIKE', "%$keyword%")
                ->orWhere('direccion', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('telefono', 'LIKE', "%$keyword%")
                ->orderBy('nombre','asc')
                ->paginate($perPage);
        } else {
            $empresa = Empresa::orderBy('nombre','asc')->paginate($perPage);
/*
            $prueba = Empresa::select(DB::raw('empresas.*,(select count(*) as cantidad
                                                from contactos
                                                inner join empresas on empresas.id = contactos.empresa_id
                                                where contactos.empresa_id = empresas.id
                                                group by empresas.id) as cantidad'))->get();

            dd($prueba);*/
        }

        return view('empresa.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('empresa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nombre' => 'required|max:200',
			'ruc' => 'max:11'
		]);
        $requestData = $request->all();
        $requestData['client_id'] = Auth::user()->client_id;
        $requestData['user_id_create'] = Auth::user()->id;

        
        Empresa::create($requestData);

        return redirect('empresa')->with('flash_message', 'Se agregpo la empresa correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $empresa = Empresa::findOrFail($id);

        return view('empresa.show', compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $empresa = Empresa::findOrFail($id);

        return view('empresa.edit', compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nombre' => 'required|max:200',
			'ruc' => 'max:11'
		]);
        $requestData = $request->all();
        
        $empresa = Empresa::findOrFail($id);
        $empresa->update($requestData);

        return redirect('empresa')->with('flash_message', 'La empresa fué editada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Empresa::destroy($id);

        return redirect('empresa')->with('flash_message', 'La empresa fue eliminada');
    }
}
