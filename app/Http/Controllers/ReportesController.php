<?php

namespace App\Http\Controllers;

use App\Empresa;
use App\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportesController extends Controller
{
    public function json()
    {

        $where = '';
        if(isset($_GET['empresa']) and $_GET['empresa']!=0){
            $where = 'and campanas.empresa_id = '.$_GET['empresa'];
        }
        $datos = DB::select('SELECT date(mensajes.created_at) as fecha, count(mensajes.id) as total from mensajes
                inner join campanas on campanas.id = mensajes.campana_id
                where mensajes.estado = 1 '.$where.'
                GROUP BY date(mensajes.created_at)
                order by 1 asc');

        $mensajes=[];
        foreach($datos as $dato){
            $mensajes[] = [strtotime($dato->fecha)*1000,(int)$dato->total];
        }

        $empresas = Empresa::all();


        return view('reporte.line',compact('mensajes','empresas'));

    }
}
