<?php

namespace App\Http\Controllers\Api;

use App\Campana;
use App\CampanaResultado;
use App\Mensaje;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MensajeController extends Controller {

    public function apiupdate($id, $estado_mensaje) {
        $mensaje = Mensaje::findOrFail($id);

        if ($estado_mensaje == 2) {
            $mensaje->estado_fecha = date('Y-m-d H:i:s');

            if($mensaje->estado_mensaje!=3){
                $mensaje->estado_mensaje = $estado_mensaje;
            }

        }
        if ($estado_mensaje == 3) {
            $mensaje->fecha_recepcion = date('Y-m-d H:i:s');
            $mensaje->estado_mensaje = $estado_mensaje;

            if($mensaje->estado_fecha == null){
                $mensaje->estado_fecha = date('Y-m-d H:i:s');
            }

        }
        $mensaje->update();
    }

    public function updateReporte(Request $request) {

        if (isset($request['reporte'])) {
            $resultados = json_decode($request['reporte']);

            foreach ($resultados as $resultado) {


                $model        = Mensaje::find($resultado->id_mensaje);
                $model->n_sms = $resultado->n_sms;

                if ($resultado->envio != null) {
                    $model->estado_mensaje = 2;
                    $time                  = strtotime($resultado->envio);
                    if ($time > 1483290000) {
                        $model->estado_fecha = date('Y-m-d H:i:s', strtotime($resultado->envio));
                    } else {
                        $model->estado_fecha = date('Y-m-d H:i:s');
                    }
                }

                if ($resultado->recepcion != null) {
                    $model->estado_mensaje = 3;
                    $time                  = strtotime($resultado->recepcion);
                    if ($time > 1483290000) {
                        $model->fecha_recepcion = date('Y-m-d H:i:s', strtotime($resultado->recepcion));
                    } else {
                        $model->fecha_recepcion = date('Y-m-d H:i:s');
                    }
                }

                $model->save();
            }
        }
    }

    public function updateEstadoFb()
    {
        $mensajes = Mensaje::select('id')->where('estado_mensaje','>',1)->where('estado_fb',1)->get();
        //dd($mensajes->toArray());

        foreach($mensajes as $mensaje){
            $model = Mensaje::find($mensaje->id);
            $model->estado_fb = 2;
            $model->update();
        }
    }

    public function envioMensajes()
    {

        /*  listar campanas que estan en proceso */

        $campanas_all = Campana::where('estado',3)->get();

        foreach($campanas_all as $camp){

            $tokens = Mensaje::select('token')->distinct()->where('campana_id',$camp->id)->whereNotNull('token')->get();

            $cant_equipos = count($tokens);

            $equipos = $tokens;

            // $reparto = $this->reparto($num_mensajes,$cant_equipos);

            $campana = Campana::find($camp->id);

            $array_fb = [
                "nombre" => $campana->nombre,
                "id"=> $camp->id,
                "espera"=>5,
                "estado"=>1,
            ];

            $reparto = 12;

            foreach($equipos as $k=>$equipo) {


                $limit =  $reparto;

                $mensajes = DB::table('mensajes')->where('campana_id',$camp->id)->where('estado',1)
                    ->where('estado_fb',0)
                    ->limit($limit)->get();


                $array_sms= [];

                foreach($mensajes as $mensaje){

                    $array_sms[] = [
                        "id" => $mensaje->id,
                        "mensaje" => $this->textoSMS($mensaje->mensaje, $campana->mensaje_base),
                        "celular" => $mensaje->numero_celular
                    ];
                    //dd($array_sms);

                    $mensaje_update = Mensaje::find($mensaje->id);
                    $mensaje_update->token = $equipo->token;
                    $mensaje_update->estado_fb = 1;
                    $mensaje_update->update();

                }

                $array_fb['conteo'] = count($array_sms);
                $array_fb['mensajes'] = $array_sms;


                if(count($array_sms)>0){
                    $urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/".$equipo->token.".json";


                    $data = json_encode($array_fb);

                    $curl = curl_init($urlFirebase);

                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST" );
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curl, CURLOPT_HEADER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER,['Content-Type:application/json']);

                    $retorno = curl_exec($curl);

                    curl_close($curl);

                    $toke_firebase = json_decode($retorno);

                    $tiempo_espera = $array_fb['espera'];
                    $agregado = $tiempo_espera*count($array_sms);
                    $ahora = date('Y-m-d H:i:s');


                    $registro = new CampanaResultado();
                    $registro->id_campana = $camp->id;
                    $registro->token = $equipo->token;
                    $registro->id_firebase = $toke_firebase->name;
                    $registro->fecha_termino = date('Y-m-d H:i:s', strtotime($ahora." +".$agregado." seconds")) ;
                    $registro->n_sms = count($array_sms);
                    $registro->estado_envio = 1;
                    $registro->save();
                }


                // }



            }
        }



    }

    public function textoSMS($jsonSMS, $templateSMS){


        if($jsonSMS==null or $jsonSMS=='' or $jsonSMS=='[null]'){
            $sms = $templateSMS;
        }else{
            $all_row = json_decode($jsonSMS);
            $mensaje = $templateSMS;

            $cant_columnas = count($all_row);

            $sms = $mensaje;
            for($i=0; $i<$cant_columnas; $i++){
                $sms = str_replace('{'.($i).'}',$all_row[$i],$sms);
            }
        }

        return $sms;

    }

}
