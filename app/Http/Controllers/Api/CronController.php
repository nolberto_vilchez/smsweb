<?php
/**
 * Created by PhpStorm.
 * User: Gonzalo
 * Date: 15/04/2018
 * Time: 04:29 AM
 */

namespace App\Http\Controllers\Api;




use App\Campana;
use App\CampanaResultado;
use App\Equipo;
use App\Mensaje;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CronController extends Controller {

    /*busca si hay campañas en proceso y sin mensajes, para cambiarkes a estado terminado*/
    public function terminator(){


        $campanas = DB::table('campanas')->where('estado',3)->get();

        foreach($campanas as $campana){

            $mensajes = DB::table('mensajes')->where('campana_id',$campana->id)->where('estado_mensaje','!=',3)->get();

            if(count($mensajes)==0){
                $camp = Campana::find($campana->id);
                $camp->estado = 4;
                $camp->save();
            }
        }


    }

    public function cron($tiempo){

        $fecha = date('Y-m-d H').':30:00';
        if($tiempo == 2){
            $n_fecha = date('Y-m-d H:i:s');
            $nuevafecha = strtotime ( '+1 hour' , strtotime ( $n_fecha ) ) ;
            $hora = date ( 'H' , $nuevafecha );
            $fecha = date('Y-m-d').' '.$hora.':00:00';
        }

        $campanas = Campana::where('estado',2)->where('fecha_envio',$fecha)->get();
        //$campanas = Campana::where('estado',2)->get();

        if(count($campanas)>0){
            $equipos = Equipo::where('activo',1)->where('client_id',1)->get();
            $num_equipos = count($equipos);

            foreach($campanas as $campana){

                /* se actualiza la campaña */
                $camp = Campana::find($campana->id);
                $camp->estado = 3;
                $camp->save();

                $n_mensajes = $campana->mensajes_total;

                $reparto = $this->reparto($n_mensajes,$num_equipos);


                $array_fb = [
                    "nombre" => $campana->nombre,
                    "id"=> $campana->id,
                    "espera"=>5,
                    "estado"=>1,
                ];





                if($reparto['n_mensajes']>12){
                    $reparto['n_mensajes']=12;
                }

                foreach($equipos as $k=>$equipo){


                    $offset =  $k * $reparto['n_mensajes'];
                    $limit =  $reparto['n_mensajes'];



                  /*  if(end($campanas) == $campanas){
                        $limit =  $reparto['n_mensajes'] + $reparto['residuo'];
                    }*/



                    $mensajes = DB::table('mensajes')->where('campana_id',$campana->id)
                                ->where('estado',1)->offset($offset)->limit($limit)->get();


                    $array_sms= [];



                    foreach($mensajes as $mensaje){


                        $array_sms[] = [
                            "id" => $mensaje->id,
                            "mensaje" => $this->textoSMS($mensaje->mensaje, $campana->mensaje_base),
                            "celular" => $mensaje->numero_celular
                        ];

                        $mensaje_update = Mensaje::find($mensaje->id);
                        $mensaje_update->token = $equipo->token;
                        $mensaje_update->estado_fb = 1;
                        $mensaje_update->update();

                    }

                    $array_fb['conteo'] = $limit;
                    $array_fb['mensajes'] = $array_sms;

                    $urlFirebase = "https://smsgateway-9ff4a.firebaseio.com/".$equipo->token.".json";

                    $data = json_encode($array_fb);

                    $curl = curl_init($urlFirebase);

                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST" );
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curl, CURLOPT_HEADER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER,['Content-Type:application/json']);

                    $retorno = curl_exec($curl);

                    curl_close($curl);

                    $toke_firebase = json_decode($retorno);

                    $tiempo_espera = $array_fb['espera'];
                    $agregado = $tiempo_espera*$limit;
                    $ahora = date('Y-m-d H:i:s');


                    $registro = new CampanaResultado();
                    $registro->id_campana = $campana->id;
                    $registro->token = $equipo->token;
                    $registro->id_firebase = $toke_firebase->name;
                    $registro->fecha_termino = date('Y-m-d H:i:s', strtotime($ahora." +".$agregado." seconds")) ;
                    $registro->n_sms = $limit;
                    $registro->estado_envio = 1;
                    $registro->save();

                    

                }


            }
        }




    }


    /*funciones internas*/
    public function textoSMS($jsonSMS, $templateSMS){


        if($jsonSMS==null or $jsonSMS=='' or $jsonSMS=='[null]'){
            $sms = $templateSMS;
        }else{
            $all_row = json_decode($jsonSMS);
            $mensaje = $templateSMS;

            $cant_columnas = count($all_row);

            $sms = $mensaje;
            for($i=0; $i<$cant_columnas; $i++){
                $sms = str_replace('{'.($i).'}',$all_row[$i],$sms);
            }
        }



        return $sms;


    }


    public function reparto($n_mensajes, $n_equipos){
        $numero = $n_mensajes/$n_equipos;
        $entera = intval($numero);
        $decimal =  $numero - $entera;
        $resto = $n_mensajes - ($entera*$n_equipos);

        return ['n_mensajes'=>$entera,'residuo'=>$resto];
    }

} 