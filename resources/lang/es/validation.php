<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute debe ser aceptado.',
    'active_url'           => ':attribute no es una URL válida.',
    'after'                => ':attribute debe ser posterior a :date.',
    'after_or_equal'       => ':attribute debe ser posterior o igual a :date.',
    'alpha'                => ':attribute solo debe contener letras.',
    'alpha_dash'           => ':attribute solo debe contener letras, números, y guiones.',
    'alpha_num'            => ':attribute solo debe contener letras y números.',
    'array'                => ':attribute debe ser un array.',
    'before'               => ':attribute debe ser antes de :date.',
    'before_or_equal'      => ':attribute debe ser antes o igual a :date.',
    'between'              => [
        'numeric' => ':attribute debe ser entre :min y :max.',
        'file'    => ':attribute debe ser entre :min y :max kilobytes.',
        'string'  => ':attribute debe ser entre :min y :max caracteres.',
        'array'   => ':attribute debe tener entre :min y :max items.',
    ],
    'boolean'              => ':attribute debe ser verdadero o falso.',
    'confirmed'            => ':attribute confirmación no coincide.',
    'date'                 => ':attribute no es una fecha válida.',
    'date_format'          => ':attribute no coincide con el formato :format.',
    'different'            => ':attribute y :other debe ser diferente.',
    'digits'               => ':attribute debe ser :digits dígitos.',
    'digits_between'       => ':attribute debe ser entre :min y :max dígitos.',
    'dimensions'           => ':attribute tiene dimensiones de imagen no válidas.',
    'distinct'             => ':attribute el campo tiene un valor duplicado.',
    'email'                => ':attribute debe ser una dirección válida.',
    'exists'               => 'selected :attribute es válido.',
    'file'                 => ':attribute debe ser un archivo.',
    'filled'               => ':attribute debe tener un valor.',
    'image'                => ':attribute debe ser una imagen.',
    'in'                   => 'selected :attribute es inválido.',
    'in_array'             => ':attribute no existe en :other.',
    'integer'              => ':attribute debe ser un entero.',
    'ip'                   => ':attribute debe ser una dirección IP.',
    'ipv4'                 => ':attribute debe ser una dirección IPv4.',
    'ipv6'                 => ':attribute debe ser una dirección IPv6.',
    'json'                 => ':attribute debe ser un JSON.',
    'max'                  => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file'    => ':attribute no debe ser mayor a :max kilobytes.',
        'string'  => ':attribute no debe ser mayor a :max caracteres.',
        'array'   => ':attribute no debe tener mas de :max items.',
    ],
    'mimes'                => ':attribute debe ser un archivo del tipo: :values.',
    'mimetypes'            => ':attribute debe ser un archivo del tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute al menos debe ser :min.',
        'file'    => ':attribute al menos debe ser :min kilobytes.',
        'string'  => ':attribute al menos debe ser :min caracteres.',
        'array'   => ':attribute debe tener al menos :min items.',
    ],
    'not_in'               => ':attribute es invalido.',
    'numeric'              => ':attribute debe ser un número.',
    'present'              => ':attribute debe estar presente.',
    'regex'                => ':attribute formato es invlaido.',
    'required'             => ':attribute es requerido.',
    'required_if'          => ':attribute es requerido cuando :other es :value.',
    'required_unless'      => ':attribute es requerido a no ser que :other este en :values.',
    'required_with'        => ':attribute es requerido cuando :values es presente.',
    'required_with_all'    => ':attribute es requerido cuando :values es presente.',
    'required_without'     => ':attribute es requerido cuando :values no es presente.',
    'required_without_all' => ':attribute es requerido cuando ninguno de :values son presentes.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => ':attribute debe ser :size.',
        'file'    => ':attribute debe ser :size kilobytes.',
        'string'  => ':attribute debe ser :size caracteres.',
        'array'   => ':attribute debe contener :size items.',
    ],
    'string'               => ':attribute debe ser un texto.',
    'timezone'             => ':attribute debe ser una zona válida.',
    'unique'               => ':attribute ya se ha tomado.',
    'uploaded'             => ':attribute no se pudo subir.',
    'url'                  => ':attribute formato es inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
