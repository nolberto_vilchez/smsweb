@extends('layouts.app2')

@section('content')
 <div class="doc forms-doc page-layout simple full-width">

        <!-- HEADER -->
        <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
            <h2 class="doc-title" id="content">Reporte</h2>

        </div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>


<script src="{{asset('Highstock/code/highstock.js')}}"></script>
<script src="{{asset('Highstock/code/modules/exporting.js')}}"></script>
<script src="{{asset('Highstock/code/modules/export-data.js')}}"></script>




<!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
                <div class="row">



        <div class="col-md-6">
        
        <form class="form-inline" action="{{route('reportes',['empresa'=>0])}}" method="get">
        <div class="form-group">

                <select name="empresa" class="form-control mr-2"  id="exampleFormControlSelect1">
                <option value="0">Todas las empresas</option>
                @foreach($empresas as $empresa)
                    <option value="{{$empresa->id}}" @if($_GET['empresa']==$empresa->id) selected @endif>{{$empresa->nombre}}</option>
                @endforeach
                </select>
                <button type="submit" class="btn btn-sm btn-primary">Filtrar</button>
            </div>
        </form>
        
        </div>




            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-body">


                        <br/>
                        <div id="container" class="container" style="height: 400px; min-width: 310px"></div>
                        <br/>



                    </div>
                </div>
            </div>




        </div>
        </div>
    </div>
</div>


		<script type="text/javascript">

 Highcharts.stockChart('container', {


        rangeSelector: {
            selected: 1
        },

        title: {
            text: 'Reporte de mensajes'
        },

        series: [{
            name: 'Mensajes diarios',
            data: {{json_encode($mensajes)}},
            marker: {
                enabled: true,
                radius: 3
            },
            shadow: true,
            tooltip: {
                valueDecimals: 2
            }
        }]
    });


		</script>
		@endsection
