<?php ?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Highstock Example</title>

		<style type="text/css">
            {{--@import "{{asset('Highstock/dark-unica.css')}}";--}}
		</style>
	</head>
	<body>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="{{asset('Highstock/code/highstock.js')}}"></script>
<script src="{{asset('Highstock/code/modules/exporting.js')}}"></script>
<script src="{{asset('Highstock/code/modules/export-data.js')}}"></script>



<div id="container" style="height: 100%; min-width: 310px"></div>


		<script type="text/javascript">
          Highcharts.setOptions({
            lang: {
                decimalPoint: ',',
                thousandsSep: '.',
                months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Outubre', 'Noviembre', 'Diciembre'],
                shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'],
                weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                loading: 'Cargando....',
                rangeSelectorFrom: 'De',
                rangeSelectorTo: 'A',
               /* rangeSelectorZoom: 'Histórico',
                resetZoom: 'Desfazer zoom',
                resetZoomTitle: 'Levar zoom para 1:1',
                exportButtonTitle: 'Exportar para imagem',
                downloadPNG: 'Baixar imagem PNG',
                downloadJPEG: 'Baixar imagem JPEG',
                downloadPDF: 'Baixar documento PDF',
                downloadSVG: 'Baixar imagem vetorial SVG',
                printChart: 'Imprimir o gráfico'*/
            },
            global: {
                useUTC: true
            }
        });

$.getJSON('http://{{$_SERVER['HTTP_HOST']}}/engie/public/json', function (data) {

    // split the data set into ohlc and volume
    var ohlc = [],
        volume = [],
        dataLength = data.length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'semana',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]],

        i = 0;

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            data[i][0], // the date
           // data[i][1], // open
            //data[i][2], // high
            //data[i][3], // low
            data[i][1] // close
        ]);

        volume.push([
            data[i][0], // the date
            data[i][2] // the volume
        ]);
    }


    // create the chart
    Highcharts.stockChart('container', {

        rangeSelector: {
            selected: 1
        },



        chart: {
                // Edit chart spacing
                spacingBottom: 15,
                spacingTop: 10,
                spacingLeft: 10,
                spacingRight: 10,
                animation: true,

                // Explicitly tell the width and height of a chart
                width: null,
                height: 500
        },

        title: {
            text: 'ENGIE'
        },

        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Cotización'
            },
            height: '60%',
            lineWidth: 2,
            resize: {
                enabled: true
            }
        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Volumen'
            },
            top: '65%',
            height: '25%',
            offset: 0,
            lineWidth: 2
        }],

        tooltip: {
            split: true,
            valueDecimals: 2,
            enabled: true,
            crosshairs: [true, true]
        },

        series: [{
            type: 'line',
            name: 'Valor',
            data: ohlc,
            dataGrouping: {
                units: groupingUnits
            },
            marker: {
                    enabled: true,
                    radius: 3
                }
        }, {
            type: 'column',
            name: 'Volume',
            data: volume,
            yAxis: 1,
            dataGrouping: {
                units: groupingUnits
            }
        }]
    });
});
		</script>
	</body>
</html>