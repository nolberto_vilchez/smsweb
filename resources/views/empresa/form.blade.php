<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ('required' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('ruc') ? 'has-error' : ''}}">
    {!! Form::label('ruc', 'Ruc', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('ruc', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required', 'onKeyPress'=>'return check(event,value)', 'onInput'=>'checkLength()'] : ['class' => 'form-control form-control-sm', 'onKeyPress'=>'return check(event,value)', 'onInput'=>'checkLength()' ]) !!}
        {!! $errors->first('ruc', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dirección') ? 'has-error' : ''}}">
    {!! Form::label('direccion', 'Dirección', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('direccion', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('dirección', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    {!! Form::label('telefono', 'Teléfono', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefono', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!isset($empresa))
    {!! Form::hidden('activo',1) !!}
@else
    <div class="form-group {{ $errors->has('activo') ? 'has-error' : ''}}">
    {!! Form::label('activo', 'Estado', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('activo', ['1'=>'Activo','2'=>'No activo'], null, ('required' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
            {!! $errors->first('activo', '<p class="help-block">:message</p>') !!}
    </div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Crear', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

<script>
function check(e,value)
{
    //Check Charater
    var unicode=e.charCode? e.charCode : e.keyCode;
    if (value.indexOf(".") != -1)if( unicode == 46 )return false;
    if (unicode!=8)if((unicode<48||unicode>57)&&unicode!=46)return false;
}
function checkLength()
{
    var fieldLength = document.getElementById('ruc').value.length;
    //Suppose u want 4 number of character
    if(fieldLength <= 11){
        return true;
    }
    else
    {
        var str = document.getElementById('ruc').value;
        str = str.substring(0, str.length - 1);
        document.getElementById('ruc').value = str;
    }
}
</script>