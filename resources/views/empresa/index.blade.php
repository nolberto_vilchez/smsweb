@extends('layouts.app2')

@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa</h2>

    </div>

    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">






            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

                        <a href="{{ url('/empresa/create') }}" class="btn btn-success btn-sm" title="Add New Empresa">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Nueva Empresa
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/empresa', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Nombre</th><th>Ruc</th><th>Dirección</th><th>Teléfono</th><th>Email</th><th>Estado</th><th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($empresa as $item)

                                @php
                                    $cantidad_contactos = \DB::table('contactos')->where('empresa_id',$item->id)->count();
                                    $cantidad_campanas = \DB::table('campanas')->where('empresa_id',$item->id)->count();
                                @endphp


                                    <tr {{ ($item->activo!=1) ? 'class=table-danger' : '' }} >
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->nombre }}</td><td>{{ $item->ruc }}</td><td>{{ $item->direccion }}</td><td>{{ $item->telefono }}</td><td>{{ $item->email }}</td><td>{{($item->activo==1)?'activo':'inactivo' }}</td>
                                        <td>
                                            <a href="{{ route('campana.empresa.lista',['empresa_id'=>$item->id]) }}" title="Campañas"><button class="btn btn-dark btn-sm"><i class="fa fa-bookmark-o" aria-hidden="true"></i> ({{$cantidad_campanas}})</button></a>
                                            <a href="{{ route('contacto.empresa.lista',['empresa_id'=>$item->id]) }}" title="Contactos"><button class="btn btn-dark btn-sm"><i class="fa fa-users" aria-hidden="true"></i> ({{$cantidad_contactos}})</button></a>
                                            {{--<a href="{{ route('contacto.empresa.crear',['empresa_id'=>$item->id]) }}" title="Contactos"><button class="btn btn-default btn-sm"><i class="fa fa-users" aria-hidden="true"></i> Crear</button></a>--}}
                                            {{--<a href="{{ url('/empresa/' . $item->id) }}" title="Mostrar Empresa"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>--}}
                                            <a href="{{ url('/empresa/' . $item->id . '/edit') }}" title="Editar Empresa"><button class="btn btn-secondary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/empresa', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Eliminar Empresa',
                                                        'onclick'=>'return confirm("¿Seguro que desea elimiar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $empresa->appends(['search' => Request::get('search')])->render() !!} </div>


                        </div>

                    </div>
                </div>
            </div>






            </div>
        </div>
    </div>
</div>
@endsection
