<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>SMS</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/favicon.ico')}}" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

    <!-- STYLESHEETS -->
    <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
        </style>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icons.css -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/icons/fuse-icon-font/style.css')}}">
    <!-- Animate.css -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendor/animate.css/animate.min.css')}}">
    <!-- Perfect Scrollbar -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" />
    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendor/fuse-html/fuse-html.min.css')}}" />
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!-- Custom CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}">


    <!-- / STYLESHEETS -->

    <!-- JAVASCRIPT -->
    <!-- jQuery -->
    <script type="text/javascript" src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>

    <!-- Perfect Scrollbar -->
    <script type="text/javascript" src="{{asset('assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <!-- popper -->
    <script type="text/javascript" src="{{asset('assets/vendor/popper.js/index.js')}}"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="{{asset('assets/vendor/bootstrap/bootstrap.min.js')}}"></script>
    <!-- Fuse Html -->
    <script type="text/javascript" src="{{asset('assets/vendor/fuse-html/fuse-html.min.js')}}"></script>
    <!-- Main JS -->
    <script type="text/javascript" src="{{asset('assets/js/main.js')}}"></script>
    <!-- / JAVASCRIPT -->
</head>

<body class="layout layout-vertical layout-left-navigation layout-below-toolbar">
    <main>
        <div id="wrapper">
            <aside id="aside" class="aside aside-left" data-fuse-bar="aside" data-fuse-bar-media-step="md" data-fuse-bar-position="left">
                <div class="aside-content-wrapper">

                    <div class="aside-content bg-primary-700 text-auto">

                        <div class="aside-toolbar">

                            <div class="logo">
                                <span class="logo-icon">S</span>
                                <span class="logo-text">SMS</span>
                            </div>

                            <button id="toggle-fold-aside-button" type="button" class="btn btn-icon d-none d-lg-block" data-fuse-aside-toggle-fold>
                                <i class="icon icon-backburger"></i>
                            </button>

                        </div>

                        <ul class="nav flex-column custom-scrollbar" id="sidenav" data-children=".nav-item">

                            <li class="subheader">
                                <span>Principal</span>
                            </li>

                          {{--  <li class="nav-item" role="tab" id="heading-dashboards">

                                <a class="nav-link ripple with-arrow collapsed" data-toggle="collapse" data-target="#collapse-dashboards" href="#" aria-expanded="false" aria-controls="collapse-dashboards">

                                    <i class="icon s-4 icon-tile-four"></i>

                                    <span>Dashboards</span>
                                </a>
                                <ul id="collapse-dashboards" class='collapse ' role="tabpanel" aria-labelledby="heading-dashboards" data-children=".nav-item">

                                    <li class="nav-item">
                                        <a class="nav-link ripple " href="index.html" data-url="user-interface-page-layouts-blank.html">

                                            <span>Project Dashboard</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link ripple " href="apps-dashboards-server.html" data-url="user-interface-page-layouts-blank.html">

                                            <span>Server</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>--}}

                            <li class="nav-item">
                                <a class="nav-link ripple " href="{{url('/home')}}" data-url="user-interface-page-layouts-blank.html">

                                    <i class="icon s-4 icon-apps"></i>

                                    <span>Dashboard</span>
                                </a>
                            </li>



                            <li class="nav-item">
                                <a class="nav-link ripple " href="{{url('/empresa')}}" data-url="user-interface-page-layouts-blank.html">
                                    <i class="icon s-4 icon-briefcase"></i>
                                    <span>Empresas</span>
                                </a>
                            </li>

                            {{--<li class="nav-item">
                                <a class="nav-link ripple " href="{{url('campana')}}" data-url="user-interface-page-layouts-blank.html">
                                    <i class="icon s-4 icon-email"></i>
                                    <span>Campañas</span>
                                </a>
                            </li>--}}

                            <li class="nav-item">
                                <a class="nav-link ripple " href="{{url('equipo')}}" data-url="user-interface-page-layouts-blank.html">
                                    <i class="icon s-4 icon-cellphone-android"></i>
                                    <span>Equipos Celulares</span>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link ripple " href="{{url('reporte/json?empresa=0')}}" data-url="user-interface-page-layouts-blank.html">

                                    <i class="icon s-4 icon-chart-line"></i>
                                    <span>Reportes</span>
                                </a>
                            </li>





                        </ul>
                    </div>
                </div>
            </aside>
            <div class="content-wrapper">
                <nav id="toolbar" class="bg-white">

                    <div class="row no-gutters align-items-center flex-nowrap">

                        <div class="col">

                            <div class="row no-gutters align-items-center flex-nowrap">

                                <button type="button" class="toggle-aside-button btn btn-icon d-block d-lg-none" data-fuse-bar-toggle="aside">
                                    <i class="icon icon-menu"></i>
                                </button>

                                <div class="toolbar-separator d-block d-lg-none"></div>





                            </div>
                        </div>

                        <div class="col-auto">

                            <div class="row no-gutters align-items-center justify-content-end">

                                <div class="user-menu-button dropdown">

                                    <div class="dropdown-toggle ripple row align-items-center no-gutters px-2 px-sm-4" role="button" id="dropdownUserMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div class="avatar-wrapper">
                                            <img class="avatar" src="{{asset('assets/images/avatars/profile.jpg')}}">
                                            <i class="status text-green icon-checkbox-marked-circle s-4"></i>
                                        </div>
                                        <span class="username mx-3 d-none d-md-block">{{ Auth::user()->name }}</span>
                                    </div>

                                    <div class="dropdown-menu" aria-labelledby="dropdownUserMenu">

<!--                                        <a class="dropdown-item" href="#">
                                            <div class="row no-gutters align-items-center flex-nowrap">
                                                <i class="icon-account"></i>
                                                <span class="px-3">My Profile</span>
                                            </div>
                                        </a>-->



                                        <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <div class="row no-gutters align-items-center flex-nowrap">
                                                <i class="icon-logout"></i> <span class="px-3">Cerrar sesión</span>

                                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </div>
                                        </a>

                                    </div>
                                </div>




                            </div>
                        </div>
                    </div>
                </nav>
                <div class="content custom-scrollbar">



                        @yield('content')



                </div>
            </div>


        </div>
    </main>
</body>

</html>