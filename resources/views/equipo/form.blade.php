<div class="form-group {{ $errors->has('marca') ? 'has-error' : ''}}">
    {!! Form::label('marca', 'Marca', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('marca', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('marca', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('modelo') ? 'has-error' : ''}}">
    {!! Form::label('modelo', 'Modelo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('modelo', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('modelo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('numero') ? 'has-error' : ''}}">
    {!! Form::label('numero', 'Número', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('numero', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('proveedor') ? 'has-error' : ''}}">
    {!! Form::label('proveedor', 'Proveedor', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('proveedor', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('proveedor', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('androidversion') ? 'has-error' : ''}}">
    {!! Form::label('androidversion', 'Android versión', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('androidversion', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('androidversion', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!isset($equipo))
    {!! Form::hidden('activo',1) !!}
@else
    <div class="form-group {{ $errors->has('activo') ? 'has-error' : ''}}">
        {!! Form::label('activo', 'Activo', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('activo', ['1'=>'Activo','2'=>'Inactivo'], null, ('required' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
            {!! $errors->first('activo', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif




<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Crear', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
