@extends('layouts.app2')

@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Equipo</h2>

    </div>

    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">






            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <a href="{{ url('/equipo/create') }}" class="btn btn-success btn-sm" title="Crear Nuevo Equipo">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar Nuevo
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/equipo', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Marca</th><th>Modelo</th><th>Número</th><th>Proveedor</th><th>Estado</th><th>Token</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($equipo as $item)
                                    <tr {{ ($item->activo!=1) ? 'class=table-danger' : '' }} >
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->marca }}</td><td>{{ $item->modelo }}</td><td>{{ $item->numero }}</td><td>{{ $item->proveedor }}</td><td>{{ $item->activo==1 ? 'Activo' : 'Inactivo' }}</td>
                                        <td>{{$item->token}}</td>
                                        <td>
                                            {{--<a href="{{ url('/equipo/' . $item->id) }}" title="Mostrar Equipo"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>--}}
                                            <a href="{{ url('/equipo/' . $item->id . '/edit') }}" title="Editar Equipo"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/equipo', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Eliminar Equipo',
                                                        'onclick'=>'return confirm("¿Seguro que desea elimiar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $equipo->appends(['search' => Request::get('search')])->render() !!} </div>

                        </div>

                    </div>
                </div>
            </div>






            </div>
        </div>
    </div>
</div>
@endsection
