@extends('layouts.app2')

@section('content')
    <div class="doc forms-doc page-layout simple full-width">

        <!-- HEADER -->
        <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
            <h2 class="doc-title" id="content">Equipo</h2>

        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
                <div class="row">








            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Equipo {{ $equipo->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/equipo') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <a href="{{ url('/equipo/' . $equipo->id . '/edit') }}" title="Edit Equipo"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['equipo', $equipo->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Equipo',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $equipo->id }}</td>
                                    </tr>
                                    <tr><th> Marca </th><td> {{ $equipo->marca }} </td></tr><tr><th> Modelo </th><td> {{ $equipo->modelo }} </td></tr><tr><th> Numero </th><td> {{ $equipo->numero }} </td></tr><tr><th> Proveedor </th><td> {{ $equipo->proveedor }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>




        </div>
        </div>
    </div>
</div>
@endsection

