@extends('layouts.app2')

@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa <small>/ campaña </small></h2>

    </div>

    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">


            <div class="col-md-12">
                <div class="card">
<div class="card-header">Empresa: <b>{{\App\Empresa::find($empresa_id)['nombre']}}</b></div>
                    <div class="card-body">
                    <a href="{{ url('/empresa') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar a Empresas</button></a>
                        <a href="{{ route('campana.empresa.crear',['empresa_id'=>$empresa_id]) }}" class="btn btn-success btn-sm" title="Crear Nueva Campaña">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar nueva campaña
                        </a>

                        {!! Form::open(['method' => 'GET', 'route' => ['campana.empresa.lista',$empresa_id], 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Mensaje base</th>
                                        <th class="text-center">Fecha Envio</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Mensajes</th>
                                        <th>sms enviados</th>
                                      
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>



                                @foreach($campana as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->nombre }}</td>
                                        <td>{{$item->mensaje_base}}</td>
                                        <td class="text-center">{{ $item->fecha_envio }}</td>
                                        <td class="text-center">
                                        @if($item->estado==1)
                                        <a style="color:#fff; text-decoration: none" href="{{route('campana.cambiarestado',['campana_id'=>$item->id])}}"><span class="badge badge-primary">Borrador</span></a>
                                        @elseif($item->estado==2)
                                         <a style="color:#fff; text-decoration: none" href="{{route('campana.cambiarestado',['campana_id'=>$item->id])}}"><span class="badge badge-info">Listo para envío</span></a>
                                        @elseif($item->estado==3)
                                        <span class="badge badge-warning">En proceso</span>
                                        @elseif($item->estado==4)
                                        <span class="badge badge-success">Terminado</span>
                                         @elseif($item->estado==5)
                                       <span class="badge badge-danger">Cancelado</span>
                                       @endif

                                        </td>
                                        <td class="text-center">{{ $item->mensajes_total }}</td>
                                        <td>
                                            @php
                                            if($item->mensajes_total!=null and ($item->estado == 3 or $item->estado == 4)){
                                                echo \App\Mensaje::where('campana_id',$item->id)->whereNotNull('estado_fecha')->count();
                                            }else{
                                                echo "-";
                                            }
                                            @endphp
                                        </td>
                                       
                                        <td>

                                            {{--<a href="{{url('mensaje/empresa/'.$empresa_id.'/campana/'.$item->id.'/cargar')}}" title="Cargar Mensajes"><button class="btn btn-primary btn-sm"><i class="fa fa-envelope-o" aria-hidden="true"></i> </button></a>--}}

{{--<a href="{{url('mensaje/empresa/'.$empresa_id.'/campana/'.$item->id.'/listar')}}" title="Listar Mensajes"><button class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i></button></a>--}}

                                           @if($item->estado==1)
                                           <a href="{{ url('/campana/' . $item->id.'/empresa/'.$empresa_id.'/editar') }}" title="Editar Campaña"><button class="btn btn-info btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                           {!! Form::open([
                                                                                           'method'=>'DELETE',
                                                                                           'url' => ['/campana', $item->id],
                                                                                           'style' => 'display:inline'
                                                                                       ]) !!}
                                                                                           {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                                                                   'type' => 'submit',
                                                                                                   'class' => 'btn btn-danger btn-sm',
                                                                                                   'title' => 'Eliminar Campaña',
                                                                                                   'onclick'=>'return confirm("¿Seguro que desea elimiar?")'
                                                                                           )) !!}
                                                                                       {!! Form::close() !!}
                                           @elseif($item->estado==2)
                                           <button data-toggle="modal" data-target="#editar_{{$item->id}}" class="btn btn-info btn-sm carga_modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                           @elseif($item->estado==3)
                                           <a href="{{route('load',['campana_id'=>$item->id,'empresa_id'=>$empresa_id,'estado'=>4,'token'=>0])}}" class="btn btn-primary btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button>
                                           @elseif($item->estado==4)
                                           <a href="{{url('mensaje/empresa/'.$empresa_id.'/campana/'.$item->id.'/listar')}}" title="Listar Mensajes"><button class="btn btn-primary btn-sm"><i class="fa fa-list" aria-hidden="true"></i></button></a>

                                           @endif

                                            {{--<a href="{{ url('/campana/'.$item->id.'/empresa/' . $empresa_id . '/editar') }}" title="Editar Campana"><button class="btn btn-secondary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>--}}
                                               <a title="reporte" href="{{route('mensaje.reporte_campana',['campana_id'=>$item->id,'empresa_id'=>$empresa_id])}}" class="btn bg-green-800 btn-sm text-auto">
                                                   <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                               </a>
                                            @if($item->estado==3)
                                                <a title="cancelar campaña" class="btn btn-danger btn-sm fuse-ripple-ready" href="{{route('campana.cancelar',['campana_id'=>$item->id])}}">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $campana->appends(['search' => Request::get('search')])->render() !!} </div>

                        </div>

                    </div>
                </div>
            </div>


 @foreach($campana as $item2)

            {{--modal de previsualizacion--}}
            <div id="editar_{{$item2->id}}" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLiveLabel">Importante</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Cuando la campaña está lista para enviar y se desea editar, este primero se va a cambiar de estado a "Borrador". <b>¿Desea modificar de estado a la camapaña seleccionada?</b></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            @if(isset($item2))

                             <a href="{{ url('/campana/' . $item2->id.'/empresa/'.$empresa_id.'/editar2') }}" title="Editar Campaña"  class="btn btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Campaña</a>

                            @endif
                        </div>
                    </div>
                </div>
            </div>

@endforeach




            </div>
        </div>
    </div>
</div>


@endsection


