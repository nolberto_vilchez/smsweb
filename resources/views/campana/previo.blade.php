@extends('layouts.app2')
<style>
.frame{
	width: 220px;
	height: 440px;
	background-color: #000;
	margin: 12px auto;
	position: relative;
	border-radius: 25px;
	box-shadow: 3px 0px 5px #333;
}
.frame .cam {
	width: 12px;
	height: 12px;
	border-radius: 50%;
	background-color: #333;
	    position: absolute;
    top: 15px;
    left: 65px;
    opacity: 0.7;
}
.frame .cam div{
	width:8px;
	height: 8px;
	border-radius: 50%;
	background-color: #294189;
	margin:2px;
}
.frame .speakers {
	display: inline-block;
	height: 5px;
	width: 65px;
	background-color: #222;
	  border-radius: 5px;
    position: absolute;
    left: 85px;
    top: 20px;
}
.phone{
	position: absolute;
	width: 200px;
	height: 335px;
	background-color:#fff;
    margin: 47px auto 0 auto;
    left: 10px;

}
.phone .top {
	color: #387ec8;
	height: 35px;
	line-height: 35px;
	background-color: #ebebec;
	border-bottom: 1px solid #000;
	padding:0px 6px;
	font-weight: bold;
}
.phone .navPhone {
	color: #387ec8;
	background-color: #f5f5f7;
	padding: 0px;
}
.phone .navPhone .circle{
	width:4px;
	height: 4px;
    background-color: #000;
    display: inline-block;
    border-radius: 50%;
    border:1px solid #000;
    margin: 0px -3.5px 4px 1px;

}
.phone .navPhone p{
	font-size:10px;
	font-weight: bold;
	display: inline-block;
	color: #000;
    margin: 2px 0.5px 0px 7px;
    position: absolute;
}
.phone .navPhone p:nth-of-type(2n){
	font-size:10px;
	font-weight: bold;
	display: inline-block;
	color: #000;
    margin: 2px -0.5px 0px 70px;
    position: absolute;
}
.phone .navPhone p:nth-of-type(3n){
	font-size:10px;
	font-weight: bold;
	display: inline-block;
	color: #000;
    margin: 2px -0.5px 0px 72px;
    position: absolute;
}
.phone .navPhone .circle:nth-of-type(5n){
	background-color: transparent;
}
.phone .navPhone .battery{
	width: 19px;
 height: 7px;
 display: inline-block;
 background-color: #fff;
  margin: 3px 0px 0px 155px;
  position: absolute;
  border-radius: 1px;
  border:1px solid #000;
}
.phone .navPhone .battery div{
  position: absolute;
  width: 12px;
 height: 7px;
 background-color: #000;
}


.msg{
	width: 150px;
	height: auto;
	background-color: #0680ff;
	border-radius: 15px;
	padding: 5px;
	margin:10px 10px;
	text-align: left;
	color: #fff;
	opacity: 0;
	animation-name: texting ;
	animation-duration: 50ms;
	animation-iteration-count: 1;
	animation-fill-mode: forwards;
	animation-delay: 1s;
}
 .frame .btn2{
 	width: 39px;
 	height: 39px;
 	border-radius: 50%;
 	background-color: #606165;
	position: absolute;
  bottom: -45px;
	left: 41%;
 }
 .frame .btn2 div{
 		width: 35px;
 	height: 35px;
 	border-radius: 50%;
 	background-color: #000;
	position: absolute;
    bottom: 2px;
    left: 2px;

 }

@keyframes texting {
	0%{
		opacity: 0;
	}
	100%{
		opacity: 1;
	}
}
@-webkit-keyframes texting {
	0%{
		opacity: 0;
	}
	100%{
		opacity: 1;
	}
}
@-moz-keyframes texting {
	0%{
		opacity: 0;
	}
	100%{
		opacity: 1;
	}
}

@media only screen and (max-width:1000px){
   .msg{
    font-size: 13px;
    margin-top: 10px;

   }
   .phone .top .messages{
     font-size:13px;
     margin-left: 5px;
   }
 }




</style>
@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa <small>/ campaña</small></h2>

    </div>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">





            <div class="col-md-12">
                <div class="card">
                     <div class="card-header">Crear nueva campaña a la empresa: <b>{{\App\Empresa::find($empresa_id)['nombre']}}</b></div>
                    <div class="card-body">

                        <button class="btn btn-warning btn-sm" onclick="history.back()"> <i class="fa fa-arrow-left" aria-hidden="true"></i> regresar</button>

                        <br />
                        <br />

                        <h6><b>Nombre de campaña: </b>{{$campana_array['nombre']}}</h6>
                        @if($tipo_envio==1)
                        <h6><b>Hora de envío: </b>{{$campana_array['fecha_envio']}}</h6>
                        @endif
                        <h6><b>Mensajes num: </b>{{$count_mensajes}}</h6>
                        <h6><b>Tipo de envio: </b>{{$tipo_envio==2 ? 'Envío inmediato' : 'Envío programado'}}</h6>




                        <div class="frame">
                        <div class="cam"> <div></div></div>
                        <div class="speakers"></div>
                        <div class="phone">
                        <div class="navPhone">
                          <div class="circle"></div>
                          <div class="circle"></div>
                          <div class="circle"></div>
                          <p>LTE</p>
                          <p class="hora">1:49 PM</p>
                          <div class="battery"><div></div></div>
                        </div>
                        <div class="top">
                        <span class="messages">Mensajes</span>
                        </div>
                        <div class="msg">{{$sms1}}</div>
                        <div class="btn2"><div></div></div>
                        </div>
</div>
                        <br>

<div class="text-center">

@if($tipo_envio==2) <!--envio directo de campaña-->
<form method="post" action="{{route('campana.empresa.envioinmediato',['empresa_id'=>$empresa_id])}}" style="display: inline-block"><!--  TODO hay que cambiarlo para envío directo -->
@elseif($tipo_envio==1)
<form method="post" action="{{route('campana.store')}}" style="display: inline-block">
@endif


                            {{ csrf_field() }}
                            <input type="hidden" name="all_row" value="{{$all_row}}"/>
                            <input type="hidden" name="mensaje_base" value="{{$mensaje_base}}"/>
                            <input type="hidden" name="nombre" value="{{$campana_array['nombre']}}"/>
                            <input type="hidden" name="fecha_envio" value="{{$campana_array['fecha_envio']}}"/>
                            <input type="hidden" name="empresa_id" value="{{$campana_array['empresa_id']}}"/>


@if($tipo_envio==2)<!--envio directo de campaña-->

<div id="checkbox_equipos">
    @foreach($equipos as $equipo)
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" name="equipo[]" type="checkbox" value="{{$equipo->id}}">
            <span class="checkbox-icon"></span>
            <span class="form-check-description">{{$equipo->numero}} / {{$equipo->marca}} - {{$equipo->modelo}} (token: {{$equipo->token}})</span>
        </label>
    </div>
    @endforeach
</div>
<br><br>

    <button id="boton_envio_inmediato" name="estado" value="1" type="submit" class="btn btn-info" data-toggle='modal' data-target='#esperando' disabled="disabled">Enviar Campaña</button>

@elseif($tipo_envio==1)
     <button name="estado" value="1" type="submit" class="btn btn-info" data-toggle='modal' data-target='#esperando'>Guardar como "Borrador"</button>
     <button name="estado" value="2" type="submit" class="btn bg-blue-grey-900 text-auto"  data-toggle='modal' data-target='#esperando'>Guardar como "Listo para envío"</button>
@endif





                           {{--modal de previsualizacion--}}
                           <div id="esperando" class="modal fade" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
                               <div class="modal-dialog" role="document">
                                   <div class="modal-content">

                                       <div class="modal-body">
                                           <h6><b>Este proceso puede tardar varios minutos,</b> por favor espere hasta que termine</h6>
                                           <img src="{{asset('preloader.gif')}}" alt=""/>
                                       </div>

                                   </div>
                               </div>
                           </div>


                        </form>
                        </div>




                      {{--  <form method="post" action="{{route('mensaje.empresa.campana.store',[$empresa_id,$campana_id])}}">
                        {{ csrf_field() }}

                        <input type="hidden" name="all_row" value="{{$all_row}}"/>
                        <input type="hidden" name="mensaje" value="{{$mensaje}}"/>

                        <button type="submit" class="btn btn-info">PROCESAR MENSAJES</button>--}}

                    </div>
                </div>
            </div>




         </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#checkbox_equipos input').click(function(){
        var checked =  $('input[name="equipo[]"]:checked').length;

        if(checked>0){
            $('#boton_envio_inmediato').prop('disabled',false);
        }else{
            $('#boton_envio_inmediato').prop('disabled',true);
        }

        });
    });

</script>
@endsection
