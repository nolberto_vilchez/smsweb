@if(isset($empresa_id))
{!! Form::hidden('empresa_id',$empresa_id) !!}
@else


        {!! Form::hidden('empresa_id', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}

@endif


<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('mensaje_base') ? 'has-error' : ''}}">
    {!! Form::label('mensaje_base', 'Mensaje', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('mensaje_base', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'rows'=>'5', 'style'=>'padding:3px 8px !important', 'required' => 'required'] : ['class' => 'form-control form-control-sm', 'rows'=>'5', 'style'=>'padding:3px 8px !important']) !!}
        <p id="contador" style="font-size: 12px; color:blue"></p>
        {!! $errors->first('mensaje_base', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('archivo') ? 'has-error' : ''}}">
    {!! Form::label('archivo', 'Archivo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
    @if(isset($crear))
     {!! Form::file('archivo', ['class' => 'form-control form-control-sm', 'required' => 'required']) !!}
    @else
     {!! Form::file('archivo',  ['class' => 'form-control form-control-sm']) !!}
    @endif

        {!! $errors->first('archivo', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<!-- radio selector de envio -->


<div class="form-check form-check-inline"  style="margin-left: 12px">
    <label class="form-check-label">
        <input class="form-check-input envio_despues" checked="checked" type="radio" name="tipo_envio" id="inlineRadio2"
               value="1"/>
        <span class="radio-icon"></span>
        <span class="" style="color:666; font-size: 13px">Envío programado</span>
    </label>
</div>


<div class="form-check form-check-inline">
    <label class="form-check-label">
        <input class="form-check-input envio_inmediato" type="radio" name="tipo_envio" id="inlineRadio1"
               value="2"/>
        <span class="radio-icon"></span>
        <span class="" style="color:666; font-size: 13px">Envío inmediato</span>
    </label>
</div>

<br>

<!-- /fin de radio selector -->



<div class="form-group {{ $errors->has('fecha_envio') ? 'has-error' : ''}}" id="fecha_envio_post">
    {!! Form::label('fecha_envio', 'Fecha Envio', ['class' => 'col-md-4 control-label']) !!}
<div class="col-md-6">

@if(isset($campana))
        <?php if($campana->fecha_envio!=''){

            $camp = explode(' ',$campana->fecha_envio);
            $campa = $camp[0].'T'.$camp[1];
        }else{
            $campa = '';
        } ?>

    <input type="datetime-local" step="1800" min="{{date('Y-m-d\TH')}}:30" name="fecha_envio" value="{{$campa}}" class="form-control" required="required"/>

@else
    <input type="datetime-local" step="1800" min="{{date('Y-m-d\TH')}}:30" name="fecha_envio" value="" class="form-control" required="required"/>
@endif
<small id="emailHelp" class="form-text text-muted">Los minutos deben ser 00 o 30</small>
                                                                </small>
        {!! $errors->first('fecha_envio', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!isset($contacto_id))
{!! Form::hidden('estado',1) !!}
@else

<div class="form-group {{ $errors->has('estado') ? 'has-error' : ''}}">
    {!! Form::label('estado', 'Estado', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('estado', json_decode('{"1":"pendiente de envio","2":"enviado"}', true), null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
    @if(isset($submitButtonText))
        {!! Form::button('Actualizar', ['class' => 'btn btn-primary', 'data-toggle'=>'modal', 'data-target'=>'#con_archivo']) !!}
    @else
        {!! Form::submit('Previsualizar Campaña', ['class' => 'btn btn-primary']) !!}
    @endif
    </div>
</div>


{{--modal de previsualizacion--}}
<div id="con_archivo" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLiveLabel">Importante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Si hay un archivo nuevo, se borrarán todos los mensajes anteriores y se volverán a crear nuevamente</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Enviar formulario</button>
            </div>
        </div>
    </div>
</div>

{{--modal de previsualizacion--}}
<div id="cargando" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLiveLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <p>Cargando...</p>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

$(".envio_inmediato").prop("checked", false);
$(".envio_despues").prop("checked", true);


/*  radio button  */
    $('.envio_inmediato').click(function() {
       if($(this).is(':checked')) {
            $('#fecha_envio_post').hide();
            $('#fecha_envio_post input').removeAttr('required');
            $('#fecha_envio_post input').val('');
        }
    });

    $('.envio_despues').click(function() {
           if($(this).is(':checked')) {
                $('#fecha_envio_post').show();
                $('#fecha_envio_post input').attr('required','required');
                $('#fecha_envio_post input').val('');
            }
        });


/* contador de caracteres */
    $('#mensaje_base').keyup(function() {
        var chars = $(this).val().length;
            var dato ='';
           if(chars==1){
           dato = ' caracter';
           }else{
           dato = ' caracteres';
           }
        $('#contador').html('Se va tipeando '+chars+dato);
    });
});

</script>



