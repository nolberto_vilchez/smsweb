@extends('layouts.app2')

@section('content')
    <div class="doc forms-doc page-layout simple full-width">

        <!-- HEADER -->
        <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
            <h2 class="doc-title" id="content">Campana</h2>

        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
                <div class="row">








            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Campana {{ $campana->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/campana') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <a href="{{ url('/campana/' . $campana->id . '/edit') }}" title="Edit Campana"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['campana', $campana->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Campana',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $campana->id }}</td>
                                    </tr>
                                    <tr><th> Empresa Id </th><td> {{ $campana->empresa_id }} </td></tr><tr><th> Nombre </th><td> {{ $campana->nombre }} </td></tr><tr><th> Fecha Envio </th><td> {{ $campana->fecha_envio }} </td></tr><tr><th> Estado </th><td> {{ $campana->estado }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>




        </div>
        </div>
    </div>
</div>
@endsection

