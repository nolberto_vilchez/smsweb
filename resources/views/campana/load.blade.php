@extends('layouts.app2')

@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa <small>/ campaña</small></h2>

    </div>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">





            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

<h5 class="text-center">PROCESANDO INFORMACIÓN <br><small>(En un momento será direccionado)</small></h5>
<div class="text-center">
<img src="{{asset('preloader.gif')}}" alt=""/>
</div>


                    </div>
                </div>
            </div>




         </div>
        </div>
    </div>
</div>

<script>



setTimeout(function(){
    window.location.replace('{{route('camapanaresultado.listar',['campana_id'=>$campana,'empresa_id'=>$empresa,'token'=>0])}}');
},10000);

</script>
@endsection
