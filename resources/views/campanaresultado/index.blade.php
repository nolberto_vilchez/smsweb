@extends('layouts.app2')

@section('content')

<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa <small>/ campaña </small></h2>

    </div>

    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">


            <div class="col-md-12">
                <div class="card">
<div class="card-header">Empresa: <b>{{\App\Empresa::find($empresa_id)['nombre']}}</b></div>
                    <div class="card-body">


                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>

                                        <th>Token del celular</th>
                                        {{--<th>Id Firebase</th>--}}
                                        <th class="text-center">Num. sms programados</th>
                                        <th class="text-center">Num. sms enviados</th>
                                        <th class="text-center">Estado envío</th>

                                    </tr>
                                </thead>
                                <tbody>




                                @foreach($resultados as $item)
                                    <tr>

                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->token }}</td>
                                        {{--<td>--}}{{--$item->id_firebase--}}{{--</td>--}}
                                        <td class="text-center">{{$item->por_enviar}}</td>
                                        <td class="text-center">
                                        @php
                                            $mensajes = \App\Mensaje::where('campana_id',$campana_id)->where('token',$item->token)->where('estado_mensaje','>=',3)->get();

                                        @endphp
                                        {{count($mensajes)}}
                                        </td>
                                        <td class="text-center">

                                            @if($item->por_enviar>count($mensajes))
                                                <span class="badge badge-warning">Pendiente</span>
                                            @else
                                                <span class="badge badge-success">Terminado</span>
                                            @endif

                                        </td>

                                        <td>
                                            @if($item->por_enviar>count($mensajes))
                                                 <a href="{{route('load',['campana_id'=>$campana_id,'empresa_id'=>$empresa_id,'estado'=>3,'token'=>$item->token])}}" class="btn btn-primary btn-sm"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                            @else
                                                 <a href="{{route('mensaje.reporte',['campana_id'=>$campana_id,'token'=>$item->token,'empresa_id'=>$empresa_id])}}" class="btn bg-green-800 btn-sm text-auto"><i class="fa fa-file-excel-o" aria-hidden="true"></i> reporte</a>
                                            @endif
                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>

                    </div>
                </div>
            </div>







            </div>
        </div>
    </div>
</div>


@endsection


