<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <title>SMS</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/favicon.ico')}}" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700italic,700,900,900italic" rel="stylesheet">

    <!-- STYLESHEETS -->
    <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
        </style>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Icons.css -->

    <!-- Fuse Html -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendor/fuse-html/fuse-html.min.css')}}" />
    <!-- Main CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!-- Custom CSS -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}">


    <!-- / STYLESHEETS -->

</head>


<body class="layout layout-vertical layout-left-navigation layout-above-toolbar">
    <main>
        <div id="wrapper">

            <div class="content-wrapper">
                <div class="content custom-scrollbar">

                    <div id="error-404" class="d-flex flex-column align-items-center justify-content-center">

                        <div class="content">

                            <div class="error-code display-1 text-center">500</div>

                            <div class="message h4 text-center text-muted">Ha surgido un problema</div>
                            <div class="message h4 text-center text-muted">Parece que tenemos un problema interno. Inténtalo de nuevo en unos minutos.</div>


<br>
<hr>
<br>


                        </div>
                    </div>

                </div>
            </div>


        </div>
    </main>
</body>

</html>