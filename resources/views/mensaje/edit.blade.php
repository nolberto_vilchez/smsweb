@extends('layouts.app2')

@section('content')
   <div class="doc forms-doc page-layout simple full-width">

       <!-- HEADER -->
       <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
           <h2 class="doc-title" id="content">Mensaje</h2>

       </div>

       @if (session('status'))
           <div class="alert alert-success">
               {{ session('status') }}
           </div>
       @endif
       <!-- / HEADER -->

       <!-- CONTENT -->
       <div class="page-content p-6">
           <div class="content container">
               <div class="row">





            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Editar Mensaje #{{ $mensaje->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/mensaje') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($mensaje, [
                            'method' => 'PATCH',
                            'url' => ['/mensaje', $mensaje->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('mensaje.form', ['submitButtonText' => 'Actualizar'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>



         </div>
        </div>
    </div>
</div>

@endsection
