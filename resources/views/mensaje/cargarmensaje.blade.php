@extends('layouts.app2')

@section('content')
   <div class="doc forms-doc page-layout simple full-width">

       <!-- HEADER -->
       <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
           <h2 class="doc-title" id="content">Empresa <small>/ campaña / mensajes / cargar</small></h2>

       </div>

       @if (session('status'))
           <div class="alert alert-success">
               {{ session('status') }}
           </div>
       @endif
       <!-- / HEADER -->

       <!-- CONTENT -->
       <div class="page-content p-6">
           <div class="content container">
               <div class="row">





            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Cargar mensajes: {{\App\Campana::find($campana_id)->nombre}}</div>
                    <div class="card-body">
                        <a href="{{ url('/campana/empresa/'.$empresa_id.'/lista') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                           {!! Form::open( [
                                'method' => 'PATCH',
                                'route' => ['mensaje.empresa.campana.enviar',$empresa_id,$campana_id],
                                'class' => 'form-horizontal',
                                'files' => true
                             ] ) !!}


                        <div class="form-group {{ $errors->has('mensaje') ? 'has-error' : ''}}">
                            {!! Form::label('mensaje', 'Mensaje', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::textarea('mensaje', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required','rows'=>'5','style'=>'padding:3px 8px !important'] : ['class' => 'form-control form-control-sm','rows'=>'5','style'=>'padding:3px 8px !important']) !!}
                                {!! $errors->first('mensaje', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('archivo') ? 'has-error' : ''}}">
                            {!! Form::label('archivo', 'Archivo', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::file('archivo', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required','style'=>'width: 100%'] : ['class' => 'form-control form-control-sm','style'=>'width: 100%;border:1px solid grey']) !!}
                                {!! $errors->first('archivo', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-4">
                                {!! Form::submit('Cargar Mensajes', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>


                        {!! Form::close() !!}

                    </div>
                </div>
            </div>



         </div>
        </div>
    </div>
</div>

@endsection
