@extends('layouts.app2')

@section('content')
    <div class="doc forms-doc page-layout simple full-width">

        <!-- HEADER -->
        <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
            <h2 class="doc-title" id="content">Mensaje</h2>

        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- / HEADER -->

        <!-- CONTENT -->
        <div class="page-content p-6">
            <div class="content container">
                <div class="row">








            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Mensaje {{ $mensaje->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/mensaje') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <a href="{{ url('/mensaje/' . $mensaje->id . '/edit') }}" title="Edit Mensaje"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['mensaje', $mensaje->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Mensaje',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $mensaje->id }}</td>
                                    </tr>
                                    <tr><th> Campana Id </th><td> {{ $mensaje->campana_id }} </td></tr><tr><th> Numero Celular </th><td> {{ $mensaje->numero_celular }} </td></tr><tr><th> Mensaje </th><td> {{ $mensaje->mensaje }} </td></tr><tr><th> Estado Mensaje </th><td> {{ $mensaje->estado_mensaje }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>




        </div>
        </div>
    </div>
</div>
@endsection

