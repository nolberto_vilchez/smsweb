@extends('layouts.app2')

@section('content')



@php


@endphp

<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Mensaje</h2>

    </div>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">






            <div class="col-md-12">
                <div class="card">
<div class="card-header">Empresa: <b>{{\App\Empresa::find($empresa_id)['nombre']}}</b> -> <b>Campaña</b>: {{\App\Campana::find($campana_id)['nombre']}}</div>
                    <div class="card-body">


                            <button class="btn btn-warning btn-sm" onclick="history.back()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button>


                         {!! Form::open(['method' => 'GET', 'route' => ['mensaje.empresa.campana.listar',$empresa_id, $campana_id], 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}

                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Número Celular</th><th>Mensaje</th><th>Estado Mensaje</th>
                                        <th>Fecha Envio</th>
                                        <th>Fecha Recepción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($mensaje as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->numero_celular }}</td></td>
                                        <td>
                                        @php
                                                $all_row = json_decode($item->mensaje);
                                                $cant_columnas = count($all_row);
                                                $sms = $mensaje_base;
                                                for($i=0; $i<$cant_columnas; $i++){
                                                    $sms = str_replace('{'.($i).'}',$all_row[$i],$sms);
                                                }
                                        @endphp
                                        {{$sms}}
                                        </td>
                                        <td>
                                            @if($item->estado_mensaje==1)
                                                {{'Pendiente'}}
                                            @elseif($item->estado_mensaje==2)
                                                {{'Enviado'}}
                                            @elseif($item->estado_mensaje==3)
                                                {{'Recibido'}}
                                            @else
                                                {{'-'}}
                                            @endif
                                        <td>{{$item->created_at}}</td>
                                        <td></td>



                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $mensaje->appends(['search' => Request::get('search')])->render() !!} </div>

                        </div>

                    </div>
                </div>
            </div>






            </div>
        </div>
    </div>
</div>
@endsection
