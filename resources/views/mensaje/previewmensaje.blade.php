@extends('layouts.app2')

@section('content')
   <div class="doc forms-doc page-layout simple full-width">

       <!-- HEADER -->
       <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
           <h2 class="doc-title" id="content">Empresa <small>/ campaña / mensajes / cargar</small></h2>

       </div>

       @if (session('status'))
           <div class="alert alert-success">
               {{ session('status') }}
           </div>
       @endif
       <!-- / HEADER -->

       <!-- CONTENT -->
       <div class="page-content p-6">
           <div class="content container">
               <div class="row">





            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Preview mensajes: {{\App\Campana::find($campana_id)->nombre}}</div>
                    <div class="card-body">
                        <a href="{{ url()->previous() }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
                        <br />
                        <br />

                        <h5>Este es un mensaje de previsualización:</h5>
                        <div style="background-color: #fafafa; border: 1px solid #eee; padding: 18px">
                            {{$sms1}}
                        </div>


<br>
                        <form method="post" action="{{route('mensaje.empresa.campana.store',[$empresa_id,$campana_id])}}">
                        {{ csrf_field() }}

                        <input type="hidden" name="all_row" value="{{$all_row}}"/>
                        <input type="hidden" name="mensaje" value="{{$mensaje}}"/>

                        <button type="submit" class="btn btn-info">PROCESAR MENSAJES</button>


                        </form>




                    </div>
                </div>
            </div>



         </div>
        </div>
    </div>
</div>

@endsection
