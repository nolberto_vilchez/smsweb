


@if(isset($empresa_id))
{!! Form::hidden('empresa_id',$empresa_id) !!}
@else
    <div class="form-group {{ $errors->has('empresa_id') ? 'has-error' : ''}}">
        {!! Form::label('empresa_id', 'Empresa Id', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('empresa_id', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
            {!! $errors->first('empresa_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
@endif


<div class="form-group {{ $errors->has('nombre') ? 'has-error' : ''}}">
    {!! Form::label('nombre', 'Nombre', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nombre', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('apellido') ? 'has-error' : ''}}">
    {!! Form::label('apellido', 'Apellido', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('apellido', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('apellido', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('telefono') ? 'has-error' : ''}}">
    {!! Form::label('telefono', 'Telefono', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('telefono', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('telefono', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('cargo') ? 'has-error' : ''}}">
    {!! Form::label('cargo', 'Cargo', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cargo', null, ('' == 'required') ? ['class' => 'form-control form-control-sm', 'required' => 'required'] : ['class' => 'form-control form-control-sm']) !!}
        {!! $errors->first('cargo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

@if(!isset($contacto_id))
{!! Form::hidden('estado',1) !!}
@else

<div class="form-group {{ $errors->has('estado') ? 'has-error' : ''}}">
    {!! Form::label('estado', 'Estado', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('estado', json_decode('{"1":"activo","2":"inactivo"}', true), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Crear', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

