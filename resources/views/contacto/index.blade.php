@extends('layouts.app2')

@section('content')
<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Empresa <small>/ contacto</small></h2>




    </div>

    @if (session('flash_message'))
        <div class="alert alert-success">
            {{ session('flash_message') }}
        </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">
            <div class="row">






            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Empresa: <b>{{\App\Empresa::find($empresa_id)['nombre']}}</b></div>
                    <div class="card-body">
                     <a href="{{ url('/empresa') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar a Empresas</button></a>
                        <a href="{{ route('contacto.empresa.crear',['empresa_id'=>$empresa_id]) }}" class="btn btn-success btn-sm" title="Crear Nuevo Contacto">
                            <i class="fa fa-plus" aria-hidden="true"></i> Agregar nuevo contacto
                        </a>

                        {!! Form::open(['method' => 'GET', 'route' => ['contacto.empresa.lista',$empresa_id], 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">

                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-sm table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Nombre</th><th>Apellido</th><th>Teléfono</th><th>Email</th><th>Cargo</th><th>Estado</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($contacto as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->nombre }}</td><td>{{ $item->apellido }}</td><td>{{ $item->telefono }}</td><td>{{ $item->email }}</td><td>{{ $item->cargo }}</td><td>{{ ($item->estado==1) ? 'activo' : 'inactivo' }}</td>
                                        <td>
                                            {{--<a href="{{ url('/contacto/' . $item->id) }}" title="Mostrar Contacto"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>--}}
                                            <a href="{{ url('/contacto/'.$item->id .'/empresa/' . $empresa_id . '/editar') }}" title="Editar Contacto"><button class="btn btn-secondary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/contacto', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Eliminar Contacto',
                                                        'onclick'=>'return confirm("¿Seguro que desea elimiar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $contacto->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>

             </div>
        </div>
    </div>
</div>
@endsection
