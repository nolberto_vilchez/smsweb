@extends('layouts.app2')

@section('content')

<style>
    .fd-tile.detail .icon {
        display: block;
        float: right;
        height: 80px;
        margin-bottom: 10px;
        padding-left: 15px;
        padding-top: 10px;
        width: 80px;
    }

    .fd-tile.detail .icon {
        display: block;
        float: right;
        height: 80px;
        margin-bottom: 10px;
        padding-left: 15px;
        padding-top: 10px;
        position: absolute;
        right: 80px;
        top: 0;
        width: 80px;
    }
    .fd-tile.detail .content {
        background: transparent;
        padding: 10px 10px 13px;
        display: inline-block;
        position: relative;
        z-index: 3;
    }

    .fd-tile .content {
        padding: 10px;
    }

    .fd-tile.detail {
        position: relative;
        overflow: hidden;
    }

    .fd-tile.clean {
        background-color: #fff;
        color: #888;
    }
    .fd-tile.tile-green {
        background-color: #19b698;
        color: #fff;
    }
    .fd-tile.detail .icon i {
        color: rgba(0, 0, 0, 0.05);
        font-size: 150px;
        line-height: 65px;
    }
    .fd-tile.clean .icon i {
        color: #e5e5e5;
    }
</style>

<div class="doc forms-doc page-layout simple full-width">

    <!-- HEADER -->
    <div class="page-header bg-secondary text-auto p-6 row no-gutters align-items-center justify-content-between">
        <h2 class="doc-title" id="content">Inicio</h2>

    </div>

    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <!-- / HEADER -->

    <!-- CONTENT -->
    <div class="page-content p-6">
        <div class="content container">

            <div class="col-12 text-center">
                <h3>Aplicacion disponible para Android 6.0 en adelante</h3>
                <img height="300" src="{{asset('assets/images/qr_code.png')}}"/>
            </div>
            <hr style="margin-top:20px; margin-bottom: 20px">
            <div class="row col-md-offset-3">
                <div class="col-2"></div>
                <div class="col-4">
                    <div class="fd-tile detail clean tile-red">
                        <div class="content"><h1 class="text-left">{{$data['equipos']}}</h1><p>Celulares Registrados</p></div>
                        <div class="icon"><i class="fa fa-mobile"></i></div>
                    </div>
                </div>
                <div class="col-1"></div>
                <div class="col-4">
                    <div class="fd-tile detail clean tile-green">
                        <div class="content"><h1 class="text-left">{{$data['empresas']}}</h1><p>Empresas Registradas</p></div>
                        <div class="icon"><i class="fa fa-building"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>




@endsection
