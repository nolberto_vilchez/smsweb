/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : smservice2

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2018-04-14 18:56:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for campanas
-- ----------------------------
DROP TABLE IF EXISTS `campanas`;
CREATE TABLE `campanas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_envio` datetime DEFAULT NULL,
  `mensaje_base` text COLLATE utf8mb4_unicode_ci,
  `estado` int(2) NOT NULL COMMENT '1:borrador; 2:listo_para_envio; 3:en proceso; 4:terminado',
  `user_id_create` int(11) DEFAULT NULL,
  `mensajes_total` int(9) DEFAULT NULL,
  `detenido` int(11) DEFAULT '0',
  `delete` int(1) DEFAULT '0',
  `fecha_delete` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of campanas
-- ----------------------------
INSERT INTO `campanas` VALUES ('1', '7', 'primera campaña', '2018-04-29 12:38:00', 'este es {0} es el primer {1} mensaje {2} a ver que sale {3} hoy', '2', '1', '7', '0', '0', null, '2018-04-14 23:41:11', '2018-04-14 23:46:46', null);

-- ----------------------------
-- Table structure for clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observaciones` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of clientes
-- ----------------------------
INSERT INTO `clientes` VALUES ('1', '2018-04-02 21:03:17', null, 'cliente 1', null);

-- ----------------------------
-- Table structure for contactos
-- ----------------------------
DROP TABLE IF EXISTS `contactos`;
CREATE TABLE `contactos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellido` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `user_id_create` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of contactos
-- ----------------------------
INSERT INTO `contactos` VALUES ('1', '2018-03-30 07:24:54', '2018-03-30 07:24:54', '1', 'Gonzalo', 'Alonso', '966734869', 'gdelcarpio@gmail.com', 'gerente general', '1', '1');
INSERT INTO `contactos` VALUES ('2', '2018-03-30 08:18:08', '2018-03-30 09:04:40', '3', 'Alonso', 'Chango', '897654897', 'alonso@chang.com', 'presidente', '1', '1');
INSERT INTO `contactos` VALUES ('4', '2018-03-30 08:38:30', '2018-04-01 06:10:37', '3', 'miguel', 'clavito', '53453453', null, 'mamifero', '1', '1');
INSERT INTO `contactos` VALUES ('5', '2018-03-30 08:45:37', '2018-03-30 08:45:37', '6', 'Renzo', 'Aliaga', '987654', null, null, '1', '1');
INSERT INTO `contactos` VALUES ('7', '2018-04-01 05:57:37', '2018-04-01 06:04:26', '1', 'Arturo', 'wars', '987654', null, 'Asesor', '1', '1');
INSERT INTO `contactos` VALUES ('8', '2018-04-01 06:11:10', '2018-04-01 06:12:42', '5', 'poblacion', 'perez', null, null, 'zapatero2', '1', '1');
INSERT INTO `contactos` VALUES ('9', '2018-04-01 15:24:46', '2018-04-01 15:24:46', '1', 'rwerewrwe', 'rwerwe', null, null, null, '1', '1');
INSERT INTO `contactos` VALUES ('10', '2018-04-01 15:25:08', '2018-04-01 15:25:08', '1', 'dsad', 'das', null, null, null, '1', '1');
INSERT INTO `contactos` VALUES ('11', '2018-04-01 15:32:49', '2018-04-01 15:32:49', '1', 'fzs', 'fsd', null, null, null, '1', '1');
INSERT INTO `contactos` VALUES ('12', '2018-04-03 02:37:55', '2018-04-03 02:37:55', '3', 'pedrito', 'perico', null, null, null, '1', '1');
INSERT INTO `contactos` VALUES ('13', '2018-04-03 02:42:02', '2018-04-03 02:42:02', '3', 'color', 'jeje4', null, null, null, '1', '1');

-- ----------------------------
-- Table structure for empresas
-- ----------------------------
DROP TABLE IF EXISTS `empresas`;
CREATE TABLE `empresas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ruc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  `client_id` int(11) DEFAULT NULL,
  `user_id_create` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of empresas
-- ----------------------------
INSERT INTO `empresas` VALUES ('3', 'perusoft', '20601313422', 'La Victoria', 'gdelcarpio@perusoft.com', '987654321', '1', '1', '1', '2018-03-30 06:39:28', '2018-03-30 06:42:28', null);
INSERT INTO `empresas` VALUES ('5', 'fdxfsd', 'fsd', null, null, null, '1', '1', '1', '2018-03-30 06:48:36', '2018-03-30 06:48:36', null);
INSERT INTO `empresas` VALUES ('6', 'insitu', null, null, null, null, '2', '1', '1', '2018-03-30 06:58:19', '2018-04-07 16:09:04', null);
INSERT INTO `empresas` VALUES ('7', 'PruebaSoft', '987654261', 'lince', 'as@sa.com', '987654531', '1', '1', '1', '2018-04-03 02:28:10', '2018-04-03 02:28:10', null);
INSERT INTO `empresas` VALUES ('11', 'gfdsgsd', '11111111111', null, null, null, '1', '1', '1', '2018-04-07 16:06:02', '2018-04-14 15:27:12', '2018-04-14 15:27:12');

-- ----------------------------
-- Table structure for equipos
-- ----------------------------
DROP TABLE IF EXISTS `equipos`;
CREATE TABLE `equipos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `marca` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modelo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proveedor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `androidversion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` int(11) DEFAULT '1',
  `token` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `user_id_create` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of equipos
-- ----------------------------
INSERT INTO `equipos` VALUES ('1', '2018-03-30 15:55:28', '2018-03-30 15:55:28', 'Samsung', 'j7', '96865352', 'claro', '7', '1', '123456', '1', '1');
INSERT INTO `equipos` VALUES ('2', '2018-03-30 16:17:40', '2018-04-01 17:18:24', 'HUAWEY', 'X9', '65874521', 'MOVISTAR', '7', '2', '654123', '1', '1');
INSERT INTO `equipos` VALUES ('12', '2018-04-07 17:57:48', '2018-04-07 17:57:48', 'Samsung', 'J5', '987654321', 'claro', '6', '1', '336104', '1', '1');
INSERT INTO `equipos` VALUES ('13', '2018-04-07 17:58:45', '2018-04-07 17:58:45', 'Samsung', 'J9', '987654321', 'claro', '6', '1', '541994', '1', '1');
INSERT INTO `equipos` VALUES ('14', '2018-04-07 18:01:46', '2018-04-07 18:01:46', 'Samsung', 'S4', '9658625', 'Entel', '6', '1', '518988', '1', '1');
INSERT INTO `equipos` VALUES ('15', '2018-04-13 22:09:26', '2018-04-13 22:09:26', 'TOYOTA', 'yaris', '654', 'claro', '7', '1', '115515', '1', '1');

-- ----------------------------
-- Table structure for mensajes
-- ----------------------------
DROP TABLE IF EXISTS `mensajes`;
CREATE TABLE `mensajes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campana_id` int(11) DEFAULT NULL,
  `numero_celular` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8mb4_unicode_ci,
  `estado_mensaje` int(11) DEFAULT NULL,
  `estado_fecha` datetime DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `user_id_create` int(11) DEFAULT NULL,
  `fecha_recepcion` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of mensajes
-- ----------------------------
INSERT INTO `mensajes` VALUES ('19', '1', '986956126', '[\"gonzalo\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('20', '1', '966734869', '[\"aldo \\\"platanin\\\"\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('21', '1', '966734869', '[\"raul\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('22', '1', '966734869', '[\"pedro\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('23', '1', '966734869', '[\"saul\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('24', '1', '987654321', '[\"lolo \\\"el mago\\\"\",\"pago\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');
INSERT INTO `mensajes` VALUES ('25', '1', '951452689', '[\"\\u00d1o\\u00f1o Per\\u00fa\",\"Pago$\"]', null, null, '1', '1', null, '2018-04-14 23:46:46', '2018-04-14 23:46:46');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('12', '2018_03_30_052212_create_empresas_table', '2');
INSERT INTO `migrations` VALUES ('13', '2018_03_30_072134_create_contactos_table', '3');
INSERT INTO `migrations` VALUES ('14', '2018_03_30_154857_create_campanas_table', '4');
INSERT INTO `migrations` VALUES ('15', '2018_03_30_155403_create_equipos_table', '5');
INSERT INTO `migrations` VALUES ('16', '2018_03_30_234603_create_mensajes_table', '6');
INSERT INTO `migrations` VALUES ('17', '2018_04_03_015656_create_clientes_table', '7');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Gonzalo', 'gdelcarpio@gmail.com', '$2y$10$yZVCd/53TmfYmbnnitX6lO6B72Uyeqf9Z1Lu16KlD0IFS6SjNIr5G', 'b2PTKYpeakJaecH01pAa3Vem34bnwjxGw3OwUer9YLQKMGPan7eUc1C2dFSW', '2018-03-29 18:37:49', '2018-03-29 18:37:49', '1');
