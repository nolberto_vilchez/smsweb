<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/


Route::get('/mensaje/update/id_mensaje/{id}/estado_mensaje/{estado_mensaje}', 'Api\MensajeController@apiupdate');
Route::post('/mensaje/update/reporte','Api\MensajeController@updateReporte');
Route::get('/mensaje/update/estadoFb','Api\MensajeController@updateEstadoFb');
Route::get('/mensaje/envio','Api\MensajeController@envioMensajes');

Route::get('/campana/cron/{tiempo}','Api\CronController@cron');
Route::get('/campana/terminator','Api\CronController@terminator');


