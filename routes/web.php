<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

/* - auth - */
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::resource('/empresa', 'EmpresaController');




Route::get('contacto/empresa/{empresa_id}/crear','ContactoController@crearcontacto')->name('contacto.empresa.crear');
Route::get('contacto/{contacto_id}/empresa/{empresa_id}/editar','ContactoController@editarcontacto')->name('contacto.empresa.editar');
Route::get('contacto/empresa/{empresa_id}/lista','ContactoController@listacontacto')->name('contacto.empresa.lista');

/* campaña */
Route::get('campana/empresa/{empresa_id}/crear','CampanaController@crearcampana')->name('campana.empresa.crear');
Route::post('campana/empresa/{empresa_id}/previo','CampanaController@previocampana')->name('campana.empresa.previo');
Route::post('campana/empresa/{empresa_id}/envioinmediato','CampanaController@envioinmediato')->name('campana.empresa.envioinmediato');

Route::get('campana/{id}/empresa/{empresa_id}/editar','CampanaController@editarcampana')->name('campana.empresa.editar');
Route::get('campana/{id}/empresa/{empresa_id}/editar2','CampanaController@editar2campana')->name('campana.empresa.editar2'); //este cambia de estado la campaña
Route::get('campana/empresa/{empresa_id}/lista','CampanaController@listacampana')->name('campana.empresa.lista');
Route::post('campana/{campana_id}/empresa/{empresa_id}/actualizar','CampanaController@actualizarcampana')->name('campana.empresa.actualizar');
Route::post('campana/{campana_id}/previoactualizar','CampanaController@previoactualizar')->name('campana.previoactualizar');
Route::get('campana/{campana_id}/cambiarestado','CampanaController@cambiarestado')->name('campana.cambiarestado');
Route::get('campana/{campana_id}/cancelar','CampanaController@cancelar')->name('campana.cancelar');


Route::resource('contacto', 'ContactoController');
Route::resource('campana', 'CampanaController');
Route::resource('equipo', 'EquipoController');
//Route::resource('mensaje', 'MensajeController');

Route::get('mensaje/empresa/{empresa_id}/campana/{campana_id}/cargar','MensajeController@cargarmensajes');
Route::patch('mensaje/empresa/{empresa_id}/campana/{campana_id}/enviar','MensajeController@enviarmensajes')->name('mensaje.empresa.campana.enviar');
Route::get('mensaje/empresa/{empresa_id}/campana/{campana_id}/enviar','MensajeController@listamensajes');
Route::get('mensaje/empresa/{empresa_id}/campana/{campana_id}/enviado','MensajeController@enviadomensajes');
Route::post('mensaje/empresa/{empresa_id}/campana/{campana_id}/store','MensajeController@storemensajes')->name('mensaje.empresa.campana.store');
Route::get('mensaje/empresa/{empresa_id}/campana/{campana_id}/listar','MensajeController@listarmensajes')->name('mensaje.empresa.campana.listar');
Route::get('mensaje/reporte/campana/{campana_id}/token/{token}/empresa/{empresa_id}','MensajeController@reporte')->name('mensaje.reporte');
Route::get('mensaje/reporte/campana/{campana_id}/empresa/{empresa_id}','MensajeController@reporte_campana')->name('mensaje.reporte_campana');


Route::get('load/campana/{campana_id}/empresa/{empresa_id}/estado/{estado}/token/{token}','CampanaController@load')->name('load');

Route::get('campanaresultado/{campana_id}/empresa/{empresa_id}','CampanaResultadoController@listar')->name('camapanaresultado.listar');

//Route::resource('cliente', 'ClienteController');

Route::get('reporte/json','ReportesController@json')->name('reportes');