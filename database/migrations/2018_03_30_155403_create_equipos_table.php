<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('marca')->nullable();
            $table->string('modelo')->nullable();
            $table->string('numero')->nullable();
            $table->string('proveedor')->nullable();
            $table->string('androidversion')->nullable();
            $table->integer('activo')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('equipos');
    }
}
