<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('campana_id')->nullable();
            $table->string('numero_celular')->nullable();
            $table->text('mensaje')->nullable();
            $table->integer('estado_mensaje')->nullable();
            $table->dateTime('estado_fecha')->nullable();
            $table->integer('estado')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mensajes');
    }
}
